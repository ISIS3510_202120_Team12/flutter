import 'package:corazon_saludable/UI/main_menu.dart';
import 'package:corazon_saludable/core/firebase/firebase_analytics.dart';
import 'package:corazon_saludable/core/firebase/flutter_fire.dart';
import 'package:corazon_saludable/features/user_basic_data/presentation/pages/user_basic_info_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

import '../injection_container.dart';
class Authentication extends StatefulWidget {
  Authentication({Key? key}) : super(key: key);

  @override
  _AuthenticationState createState() => _AuthenticationState();
}

class _AuthenticationState extends State<Authentication> {
  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(body:Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: const BoxDecoration(
          color: Color(0xff002d72)
      ),
      child: FormBuilder(key: _formKey, child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container( width: MediaQuery.of(context).size.width / 1.3,
              child: FormBuilderTextField(
                name: "email",
                decoration: const InputDecoration(
                    labelText: "Inserta tu correo electrónico",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    filled: true,
                    hintStyle: TextStyle(color: Color(0xFF424242)),
                    hintText: "tucorreo@gmail.com",
                    fillColor: Colors.white70),
                validator: FormBuilderValidators.email(context), )
          ),
          SizedBox(height: MediaQuery.of(context).size.height / 35),

          Container( width: MediaQuery.of(context).size.width / 1.3,
              child: FormBuilderTextField(
                name: "contrasenia",
                obscureText: true,
                decoration: const InputDecoration(
                    labelText: "Tu contraseña",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    filled: true,
                    hintStyle: TextStyle(color: Color(0xFF424242)),
                    hintText: "**************",
                    fillColor: Colors.white70),
                validator: FormBuilderValidators.minLength(context, 8), )
          ),
          SizedBox(height: MediaQuery.of(context).size.height / 35),
          SizedBox(height: MediaQuery.of(context).size.height / 35),
          Container(
            width: MediaQuery.of(context).size.width / 1.4,
            height: 45,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              color: Colors.white,
            ),
            child: MaterialButton(
              onPressed: () async {
                final validationSucess = _formKey.currentState!.validate();
                if(validationSucess){
                  _formKey.currentState?.save();
                  final formData = _formKey.currentState?.value;
                    bool shouldNavigate = await signIn(formData!["email"], formData!["contrasenia"]);
                    if(shouldNavigate){
                      sl<AnalyticService>().registerLogin();
                      Navigator.push(context, MaterialPageRoute(builder: (context) => MainMenu(),
                      ),
                      );
                    }
                    else{
                      showDialog<void>(
                        context: context,
                        barrierDismissible: false,
                        // false = user must tap button, true = tap outside dialog
                        builder: (BuildContext dialogContext) {
                          return AlertDialog(
                            title: const Text("Información Incorrecta"),
                            content: const Text(
                                "Usuario o contraseña incorrecta"),
                            actions: <Widget>[
                              TextButton(
                                child: const Text('Entendido'),
                                onPressed: () {
                                  sl<AnalyticService>().registerLogin();
                                  Navigator.of(dialogContext)
                                      .pop(); // Dismiss alert dialog
                                },
                              ),
                            ],
                            elevation: 24,
                            backgroundColor: Colors.white,
                          );
                        },
                      );
                    }

                }

              },
              child: Text("Inicia sesión"),
            ),
          ),
          SizedBox(height: MediaQuery.of(context).size.height / 35),
        ],
      ))
      ,
    ),
    );
  }

}
