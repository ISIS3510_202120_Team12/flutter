import 'package:corazon_saludable/UI/sign_in_sign_up_menu.dart';
import 'package:corazon_saludable/core/firebase/firebase_analytics.dart';
import 'package:corazon_saludable/features/heart_monitor/heart_bpm_monitor.dart';
import 'package:corazon_saludable/features/user_basic_data/presentation/pages/user_basic_info_page.dart';
import 'package:corazon_saludable/features/user_preferences/presentation/pages/user_preferences_page.dart';
import 'package:corazon_saludable/features/user_tips/presentation/pages/user_tips_page.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';

import '../injection_container.dart';

class MainMenu extends StatelessWidget {
  const MainMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: [
            Container(
              height: 155,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                  bottomRight: Radius.circular(50),
                ),
                color: const Color(0xff002d72),
                boxShadow: [
                  BoxShadow(
                      color: const Color(0xff002d72).withOpacity(0.3),
                      offset: const Offset(-10.0, 0.0),
                      blurRadius: 20.0,
                      spreadRadius: 4.0)
                ],
              ),
              child: Stack(
                children: [
                  Positioned(
                      top: 60,
                      left: 0,
                      child: Container(
                        height: 80,
                        width: 300,
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(50),
                              bottomRight: Radius.circular(50),
                            )),
                      )),
                  const Positioned(
                    top: 85,
                    left: 20,
                    child: Text(
                      "Bienvenido",
                      style: TextStyle(
                        fontSize: 30,
                        color: Color(0xffe30046),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: height * 0.05,
            ),
            //!Rodrigo: el sizedBox de abajo era un container, cambiar de vuelta si vuelve a molestar
            SizedBox(
              height: 210,
              child: InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) =>  UserTipsPage(iniciar: true),  settings: const RouteSettings(name: "UserTips")));
                  print ("AAA");
                },
                child: Stack(
                  children: [
                    Positioned(
                      top: 35,
                      left: 20,
                      child: Material(
                        child: Container(
                          height: 160.0,
                          width: width * 0.9,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(0.0),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.3),
                                  offset: const Offset(-10.0, 10.0),
                                  blurRadius: 20.0,
                                  spreadRadius: 4.0)
                            ],
                          ),
                        ),

                      ),),
                    Positioned(
                        top: 0,
                        left: 30,
                        child: Card(
                          color: const Color(0xffffffff),
                          elevation: 10.0,
                          shadowColor: Colors.grey.withOpacity(0.5),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0)),
                          child: Container(
                            height: 180,
                            width: 150,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                image: const DecorationImage(
                                    fit: BoxFit.fill,
                                    image: AssetImage(
                                        "lib/core/images/tips.png"))),
                          ),

                        )),
                    Positioned(
                        top: 58,
                        left: 200,
                        child: Container(
                          height: 160,
                          width: 180, //! puede ser cambiada por 160 para ver que la barra no moleste
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Actividad Física",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Color(0xffe30046),
                                    fontWeight: FontWeight.bold),
                              ),
                              const Text(
                                "Trotar 20 minutos",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold),
                              ),
                              Divider(color: Colors.grey.shade500,),
                              const Text(
                                "Intermedia",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),

                        )),

                  ],
                ),
              )
            ),
            //!Rodrigo: este expanded box se va a tratar de trasladar mas arriba para que la parte de la primera card tambien sea scrollable
            Expanded(
              child: MediaQuery.removePadding(context: context,
                removeTop: true,
                child: ListView(
              children: [
                Container(
                margin: const EdgeInsets.only(bottom: 5, top: 1),
                height: 160,
                padding: const EdgeInsets.only(left: 20, right: 20, bottom: 15),
                child: Material(
                  child: InkWell(
                      splashColor: Colors.white,
                      child: Container(
                    decoration:  BoxDecoration(
                        color: const Color(0xff002d72),
                        borderRadius: const BorderRadius.only(
                          bottomLeft: Radius.circular(80.0),
                        ),
                        boxShadow: [BoxShadow(
                            color: const Color(0xff002d72).withOpacity(0.3),
                            offset: const Offset(-10.0, 0.0),
                            blurRadius: 20.0,
                            spreadRadius: 4.0
                        )]
                    ),
                    padding: const EdgeInsets.only(
                      left: 32.0,
                      top: 50.0,
                      bottom: 50,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Text("Biometría", style: TextStyle(
                            color: Colors.white,
                            fontSize: 12),),
                        SizedBox(
                          height: 2,
                        ),
                        Text("Revisa tu corazón",
                          style: TextStyle(fontSize: 22,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),)
                      ],
                    ),
                  ),
                      onTap: (){
                        //! Rodrigo, we are adding name to the routes
                        ///the analytic service is called to track
                        ///the route followed by the user
                    Navigator.push(context, MaterialPageRoute(builder: (context) =>  HeartBPM(), settings: const RouteSettings(name: "HeartBPM")));} ,)
                    , color: const Color(0xffe30046)
                )
                ,
              ),
                Container(
                  margin: const EdgeInsets.only(bottom: 5, top: 1),
                  height: 160,
                  padding: const EdgeInsets.only(left: 20, right: 20, bottom: 15),
                  child: Material( child:InkWell(
                    splashColor: Colors.white,
                    child:  Container(
                      decoration:  BoxDecoration(
                          color: const Color(0xff002d72),
                          borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(80.0),
                          ),
                          boxShadow: [BoxShadow(
                              color: const Color(0xff002d72).withOpacity(0.3),
                              offset: const Offset(-10.0, 0.0),
                              blurRadius: 20.0,
                              spreadRadius: 4.0
                          )]
                      ),
                      padding: const EdgeInsets.only(
                        left: 32.0,
                        top: 50.0,
                        bottom: 50,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                          Text("Mis datos básicos", style: TextStyle(
                              color: Colors.white,
                              fontSize: 12),),
                          SizedBox(
                            height: 2,
                          ),
                          Text("Actualiza tu información",
                            style: TextStyle(fontSize: 22,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),)
                        ],
                      ),
                    ),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) =>  UserBasicDataPage(iniciar: true),  settings: const RouteSettings(name: "UserBasicData")));
                    } ,
                  ) ,
                      color: const Color(0xffe30046))
                ),
                Container(
                    margin: const EdgeInsets.only(bottom: 5, top: 1),
                    height: 160,
                    padding: const EdgeInsets.only(left: 20, right: 20, bottom: 15),
                    child: Material( child:InkWell(
                      splashColor: Colors.white,
                      child:  Container(
                        decoration:  BoxDecoration(
                            color: const Color(0xff002d72),
                            borderRadius: const BorderRadius.only(
                              topRight: Radius.circular(80.0),
                            ),
                            boxShadow: [BoxShadow(
                                color: const Color(0xff002d72).withOpacity(0.3),
                                offset: const Offset(-10.0, 0.0),
                                blurRadius: 20.0,
                                spreadRadius: 4.0
                            )]
                        ),
                        padding: const EdgeInsets.only(
                          left: 32.0,
                          top: 50.0,
                          bottom: 50,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: const [
                            Text("Mis Preferencias", style: TextStyle(
                                color: Colors.white,
                                fontSize: 12),),
                            SizedBox(
                              height: 2,
                            ),
                            Text("Revisa tus preferencias",
                              style: TextStyle(fontSize: 22,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),)
                          ],
                        ),
                      ),
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) =>  UserPreferencesPage(iniciar: true),  settings: const RouteSettings(name: "UserPreferences")));
                      } ,
                    ) ,
                        color: const Color(0xffe30046))
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 5, top: 1),
                  height: 160,
                  padding: const EdgeInsets.only(left: 20, right: 20, bottom: 15),
                  child: Material(
                      child: InkWell(
                        splashColor: Colors.white,
                        child: Container(
                          decoration:  BoxDecoration(
                              color: const Color(0xff002d72),
                              borderRadius: const BorderRadius.only(
                                bottomLeft: Radius.circular(80.0),
                              ),
                              color: const Color(0xffe30046)),
                        ),
                        Container(
                            margin: const EdgeInsets.only(bottom: 5, top: 1),
                            height: 160,
                            padding: const EdgeInsets.only(
                                left: 20, right: 20, bottom: 15),
                            child: Material(
                                child: InkWell(
                                  splashColor: Colors.white,
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: const Color(0xff002d72),
                                        borderRadius: const BorderRadius.only(
                                          topRight: Radius.circular(80.0),
                                        ),
                                        boxShadow: [
                                          BoxShadow(
                                              color: const Color(0xff002d72)
                                                  .withOpacity(0.3),
                                              offset: const Offset(-10.0, 0.0),
                                              blurRadius: 20.0,
                                              spreadRadius: 4.0)
                                        ]),
                                    padding: const EdgeInsets.only(
                                      left: 32.0,
                                      top: 50.0,
                                      bottom: 50,
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: const [
                                        Text(
                                          "Mis datos básicos",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 12),
                                        ),
                                        SizedBox(
                                          height: 2,
                                        ),
                                        Text(
                                          "Actualiza tu información",
                                          style: TextStyle(
                                              fontSize: 22,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    ),
                                  ),
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                UserBasicDataPage(
                                                    iniciar: true),
                                            settings: const RouteSettings(
                                                name: "UserBasicData")));
                                  },
                                ),
                                color: const Color(0xffe30046))),
                        Container(
                            margin: const EdgeInsets.only(bottom: 5, top: 1),
                            height: 160,
                            padding: const EdgeInsets.only(
                                left: 20, right: 20, bottom: 15),
                            child: Material(
                                child: InkWell(
                                  splashColor: Colors.white,
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: const Color(0xff002d72),
                                        borderRadius: const BorderRadius.only(
                                          topRight: Radius.circular(80.0),
                                        ),
                                        boxShadow: [
                                          BoxShadow(
                                              color: const Color(0xff002d72)
                                                  .withOpacity(0.3),
                                              offset: const Offset(-10.0, 0.0),
                                              blurRadius: 20.0,
                                              spreadRadius: 4.0)
                                        ]),
                                    padding: const EdgeInsets.only(
                                      left: 32.0,
                                      top: 50.0,
                                      bottom: 50,
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: const [
                                        Text(
                                          "Mis Preferencias",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 12),
                                        ),
                                        SizedBox(
                                          height: 2,
                                        ),
                                        Text(
                                          "Actualiza tus Preferencias",
                                          style: TextStyle(
                                              fontSize: 22,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    ),
                                  ),
                                  onTap: () {
                                    print("Entrar a Preferencias");
                                    //Navigator.push(context, MaterialPageRoute(builder: (context) =>  UserPreferencesPage(iniciar: true),  settings: const RouteSettings(name: "UserPreferences")));
                                  },
                                ),
                                color: const Color(0xffe30046))),
                        Container(
                            margin: const EdgeInsets.only(bottom: 5, top: 1),
                            height: 160,
                            padding: const EdgeInsets.only(
                                left: 20, right: 20, bottom: 15),
                            child: Material(
                                child: InkWell(
                                  splashColor: Colors.white,
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: const Color(0xff002d72),
                                        borderRadius: const BorderRadius.only(
                                          topRight: Radius.circular(80.0),
                                        ),
                                        boxShadow: [
                                          BoxShadow(
                                              color: const Color(0xff002d72)
                                                  .withOpacity(0.3),
                                              offset: const Offset(-10.0, 0.0),
                                              blurRadius: 20.0,
                                              spreadRadius: 4.0)
                                        ]),
                                    padding: const EdgeInsets.only(
                                      left: 32.0,
                                      top: 50.0,
                                      bottom: 50,
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: const [
                                        Text(
                                          "Consulta tus datos",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 12),
                                        ),
                                        SizedBox(
                                          height: 2,
                                        ),
                                        Text(
                                          "Tu Información Médica",
                                          style: TextStyle(
                                              fontSize: 22,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    ),
                                  ),
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                UserInfoMedPage(iniciar: true),
                                            settings: const RouteSettings(
                                                name: "UserInfoMed")));
                                  },
                                ),
                                color: const Color(0xffe30046))),
                        Container(
                          margin: const EdgeInsets.only(bottom: 5, top: 1),
                          height: 160,
                          padding: const EdgeInsets.only(
                              left: 20, right: 20, bottom: 15),
                          child: Material(
                              child: InkWell(
                                splashColor: Colors.white,
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: const Color(0xff002d72),
                                      borderRadius: const BorderRadius.only(
                                        bottomLeft: Radius.circular(80.0),
                                      ),
                                      boxShadow: [
                                        BoxShadow(
                                            color: const Color(0xff002d72)
                                                .withOpacity(0.3),
                                            offset: const Offset(-10.0, 0.0),
                                            blurRadius: 20.0,
                                            spreadRadius: 4.0)
                                      ]),
                                  padding: const EdgeInsets.only(
                                    left: 32.0,
                                    top: 50.0,
                                    bottom: 50,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: const [
                                      Text(
                                        "Esperamos vuelvas pronto",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 12),
                                      ),
                                      SizedBox(
                                        height: 2,
                                      ),
                                      Text(
                                        "Cerrar sesión",
                                        style: TextStyle(
                                            fontSize: 22,
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                      )
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  //! Rodrigo, we are adding name to the routes
                                  ///the analytic service is called to track
                                  ///the route followed by the user
                                  Navigator.pop(context);
                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              SignInSignUpMenu()));
                                },
                              ),
                              color: const Color(0xffe30046)),
                        ),
                        const SizedBox(
                          height: 20,
                        )
                      ],
                    )))
          ],
        ));
  }
}
