import 'package:corazon_saludable/UI/authentication.dart';
import 'package:corazon_saludable/UI/main_menu.dart';
import 'package:corazon_saludable/UI/sign_up.dart';
import 'package:corazon_saludable/core/firebase/firebase_analytics.dart';
import 'package:corazon_saludable/core/firebase/flutter_fire.dart';
import 'package:corazon_saludable/features/user_basic_data/presentation/pages/user_basic_info_page.dart';
import 'package:flutter/material.dart';

import '../injection_container.dart';
/// this is defined as te main page for the app,
/// it allows the user to choose if he wants to create a new account or Login
/// with an existing one
class SignInSignUpMenu extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(body:Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: const BoxDecoration(
          color: Color(0xff002d72)
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          //!insertar el texto
          Text("Corazón saludable", style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.white),),
          SizedBox(height: MediaQuery.of(context).size.height / 35),
          //! insertar la imagen
          Container(
            height: 180,
            width: 150,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                image: const DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage(
                        "lib/core/images/splash.png"))),
          ),
          SizedBox(height: MediaQuery.of(context).size.height / 17),
          Container(
            width: MediaQuery.of(context).size.width / 1.4,
            height: 45,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              color: Colors.white,
            ),
            child: MaterialButton(
              onPressed: () async {
                Navigator.push(context, MaterialPageRoute(builder: (context) => Authentication()));
              },
              child: Text("Inicia Sesión"),
            ),
          ),
          SizedBox(height: MediaQuery.of(context).size.height / 35),
          Container(
            width: MediaQuery.of(context).size.width / 1.4,
            height: 45,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              color: Colors.white,
            ),
            child: MaterialButton(
              onPressed: () async {
                Navigator.push(context, MaterialPageRoute(builder: (context) => SignUp()));
              },
              child: Text("Regístrate"),
            ),

          ),
        ],
      ),
    ),
    );
  }

}
