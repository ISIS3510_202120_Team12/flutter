import 'package:corazon_saludable/features/user_basic_data/data/data_sources/user_basic_info_remote_data_source.dart';
import 'package:corazon_saludable/features/user_basic_data/domain/entities/user_basic_info.dart';
import 'package:http/http.dart' as http;
import '../../injection_container.dart';

/// Rodrigo: the following class will be used to keep the data in memory and avoid to reading data from storage.
class UserBasicDataCache {
  bool initialized = false;
  http.Client cliente = sl<http.Client>();
  late UserBasicInfo cache;
  UserBasicInfo obtainStoredCache(){
    return cache;
  }
  void storeCache(UserBasicInfo toStore){
    cache = toStore;
    initialized = true;
  }

  void initialize(){
    var valor = UserBasicInfoRemoteDataSourceImpl(cliente).getUserBasicInfo();
    valor.then((value) => cache = value);

  }
}