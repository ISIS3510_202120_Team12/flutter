import "package:firebase_analytics/firebase_analytics.dart";
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/cupertino.dart';
/// Rodrigo: the following class administrates the services used
/// with firebase analytics.
class AnalyticService{
  final FirebaseAnalytics _analytics = FirebaseAnalytics();
  FirebaseAnalyticsObserver getAnalyticsObserver() =>  FirebaseAnalyticsObserver(analytics: _analytics);
  ///Rodrigo: This class is used to set the id of the user that is currently
  ///using the app
  Future setUserProperties({required String userId})async{
    await _analytics.setUserId(userId);
  }
  ///Rodrigo: The following is a method that registers when an specific
  ///user data has been updated, it receives the name of the document that
  ///we are updating as a parameter.
  Future dataUpdated({required String nombre}) async {
    await _analytics.logEvent(name: "updated", parameters:{ "itemUpdated": nombre });
  }
  ///Rodrigo: this method registers the login, the logind method can
  ///be added as a parameter, but is not needed, for now
  Future registerLogin() async{
    await _analytics.logLogin();
  }
  Future signUp()async{
    await _analytics.logSignUp(signUpMethod: "Email");
  }
  ///Rodrigo:this method throws an event that registers when the final user
  ///uses the heart monitor functionality and registers its last value
  Future registerStartHeartMonitoring({required int lastRecordedData}) async {
    await _analytics.logEvent(name: "heartBPM_LastMesurement", parameters: {"ultimaMedicion": lastRecordedData });
  }
}