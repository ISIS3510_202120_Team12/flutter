import 'package:firebase_auth/firebase_auth.dart';

Future<String> getUserId() async{
  String id = "";
  id = await FirebaseAuth.instance.currentUser!.uid;
  return id;
}
Future<bool> signIn(String email, String password) async {
  try{
    await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password);
    return true;
  } catch(e){
    print(e);
    return false;
  }
}
Future<bool> register(String email, String password) async{
  try{
    await FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: password);
    return true; // retorna true en caso de que se pueda crear la cuenta
  } on FirebaseAuthException catch (e){
    if(e.code == 'weak-password'){
      print('La contraseña insertada es muy débil.');
    } else if (e.code == 'email-already-in-use'){
      print('Ya existe una cuenta para este mail.');
    }
    return false;
  } catch(e){
    print(e.toString());
    return false;
  }
}