import 'dart:convert';
import 'package:corazon_saludable/core/error/exeptions.dart';
import 'package:corazon_saludable/features/user_basic_data/data/models/user_basic_info_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class UserBasicInfoLocalDataSource {
  /// Gets the cached [UserBasicInfoModel] which was gotten the last time
  /// the user had an internet connection.
  ///
  /// Throws [CacheException] if no cached data is present.
  Future<UserBasicInfoModel> getLastBasicUserData();

  Future<void> cacheBasicUserData(UserBasicInfoModel userDataToCache);
}

const CACHED_USER_BASIC_DATA = 'CACHED_USER_BASIC_DATA';

class UserBasicInfoLocalDataSourceImpl implements UserBasicInfoLocalDataSource {
  final SharedPreferences sharedPreferences;

  UserBasicInfoLocalDataSourceImpl({required SharedPreferences preferences})
      : sharedPreferences = preferences;

  @override
  Future<void> cacheBasicUserData(UserBasicInfoModel userDataToCache) {
    //!TODO this method will have to print a dictionary coded as a json String.

    return sharedPreferences.setString(
        CACHED_USER_BASIC_DATA, json.encode(userDataToCache.toJson()));
  }

  @override
  Future<UserBasicInfoModel> getLastBasicUserData() {
    final jsonString = sharedPreferences.getString(CACHED_USER_BASIC_DATA);
    if (jsonString != null) {
      return Future.value(UserBasicInfoModel.fromJson(json.decode(jsonString)));
    } else {
      throw CacheException();
    }
  }
}
