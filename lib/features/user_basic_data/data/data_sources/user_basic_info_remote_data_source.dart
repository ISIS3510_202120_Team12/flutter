import 'dart:convert';
import 'package:corazon_saludable/core/error/exeptions.dart';
import 'package:corazon_saludable/features/user_basic_data/data/models/user_basic_info_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;
abstract class UserBasicInfoRemoteDataSource{
  /// Calls the https://us-central1-backendcorazonsaludable.cloudfunctions.net/user/{number} endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<UserBasicInfoModel> getUserBasicInfo();

  /// Posts new user info https://us-central1-backendcorazonsaludable.cloudfunctions.net/user/{number}endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<UserBasicInfoModel> addUserBasicInfo(UserBasicInfoModel modelo);

}


class UserBasicInfoRemoteDataSourceImpl implements UserBasicInfoRemoteDataSource{
  final http.Client client;
  String? logueado =  FirebaseAuth.instance.currentUser?.uid;
  late String apiUrl = "https://us-central1-backendcorazonsaludable.cloudfunctions.net/user/$logueado";
  //Rodrigo:  newer versions of the http library require use Uri data type instead
  //of the a string with
  late Uri uriApi = Uri.parse(apiUrl);
  UserBasicInfoRemoteDataSourceImpl(this.client);

  @override
  Future<UserBasicInfoModel> addUserBasicInfo(UserBasicInfoModel modelo) async {
    //TODO: Eliminar los prints de debugs del código final
    print("voy a empezar a hacer el put de userBasicInfo");
    final response  = await client.put(uriApi,headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    }, body: json.encode(modelo.toJson()) );
    //TODO: Eliminar los prints de debugs del código final
    print("terminé de hacer el put, voy a verificar mi código");
    if(response.statusCode == 201){
      return modelo;
    } else {
      throw ServerException();
    }
  }
  ///!Rodrigo: this method brings the data from the remote data source
  ///which is received as a json that then is used to create the UserBasicInfoModel
  ///it @throws a server exception when the request code is different from 200
  @override
  Future<UserBasicInfoModel> getUserBasicInfo() async {
    final response = await client.get(uriApi,
    headers: {
      'Content-Type': 'application/json',
    },);
    if (response.statusCode ==200){
      return UserBasicInfoModel.fromJson(json.decode(response.body)['body']);
    } else {
      throw ServerException();
    }
  }
  

}
