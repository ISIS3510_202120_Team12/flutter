import 'package:cloud_firestore_platform_interface/src/timestamp.dart';
import 'package:corazon_saludable/features/user_basic_data/domain/entities/user_basic_info.dart';
import 'package:flutter/physics.dart';
import 'package:meta/meta.dart';
import 'dart:developer';
import 'package:intl/intl.dart';

class UserBasicInfoModel extends UserBasicInfo{
  UserBasicInfoModel(
      String deptoCardio,
      bool empleado,
      //TODO: validar el testing para convertir desde json
      DateTime fechaNacimiento,
      String nombreCompleto,
      int rol,
      String sexo
      )
      : super(deptoCardio, empleado, fechaNacimiento, nombreCompleto, rol, sexo);

factory UserBasicInfoModel.fromJson(Map<String, dynamic> json){
  return UserBasicInfoModel(
      json['deptoCardio'],
      json['empleado'] as bool,
       //! "d/M/yyyy"
       DateTime.parse(json['fechaNacimiento'].toString().split("T")[0]),
      json['nombreCompleto'],
      (json['rol'] as num).toInt(),
      json['sexo']);

}

Map<String, dynamic> toJson(){
return {
  "deptoCardio": deptoCardio,
  "empleado": empleado,
  "fechaNacimiento": fechaNacimiento.toIso8601String(),
  "nombreCompleto":nombreCompleto,
  "rol":rol,
  "sexo": sexo,

};
}


}
/**
 * ejemplo para manejar timestamps

var cosito = {"deptoCardio":"el que toque prro",
  "empleado": false,
  "fechaNacimiento": 1633638870,
  "nombreCompleto": "Jordi El niño Polla",
  "rol": "el que quieras mami",
  "sexo": "si, gracias"};
void main(){
  UserBasicInfoModel modelin = UserBasicInfoModel.fromJson(cosito);
  print(modelin.toString());
}
 */