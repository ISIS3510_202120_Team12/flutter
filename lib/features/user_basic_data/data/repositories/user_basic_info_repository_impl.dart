import 'package:corazon_saludable/core/error/exeptions.dart';
import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/network/network_info.dart';
import 'package:corazon_saludable/features/user_basic_data/data/data_sources/user_basic_info_local_data_source.dart';
import 'package:corazon_saludable/features/user_basic_data/data/data_sources/user_basic_info_remote_data_source.dart';
import 'package:corazon_saludable/features/user_basic_data/data/models/user_basic_info_model.dart';
import 'package:corazon_saludable/features/user_basic_data/domain/entities/user_basic_info.dart';
import 'package:corazon_saludable/features/user_basic_data/domain/repositories/user_basic_info_repository.dart';
import 'package:dartz/dartz.dart';

class UserBasicInfoRepositoryImpl implements UserBasicInfoRepository {
  //reference of the local data source
  final UserBasicInfoLocalDataSource localDataSource;

//reference of the remote data source
  final UserBasicInfoRemoteDataSource remoteDataSource;

//reference to network info, service that checks is the device is onLine
  final NetworkInfo networkInfo;

  UserBasicInfoRepositoryImpl(
      this.localDataSource, this.remoteDataSource, this.networkInfo);

  @override
  ///Rodrigo: This method tries to obtain the data from the remote repository when needed
  ///and duplicates it in the local repository
  ///if there is no internet connection, it relies in the data stored in the cache.
  Future<Either<Failure, UserBasicInfo>> getUserBasicInfo() async {
    if (await networkInfo.isConnected) {
      try {
        UserBasicInfoModel remoteUserData = await remoteDataSource.getUserBasicInfo();
        localDataSource.cacheBasicUserData(remoteUserData);
        return Right(remoteUserData);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localUserData = await localDataSource.getLastBasicUserData();
        return Right(localUserData);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }
  /// Rodrigo: this method uploads the user basic data of the used into the cloud
  /// also saves a local copy in the shared preferences
  //TODO: correct this monstrosity against the humanity
  @override
  Future<Either<Failure, UserBasicInfo>> setUserData(userBasicInfo) async {
    if (await networkInfo.isConnected) {
      try {
        final modelo = UserBasicInfoModel(
            userBasicInfo.deptoCardio,
            userBasicInfo.empleado,
            userBasicInfo.fechaNacimiento,
            userBasicInfo.nombreCompleto,
            userBasicInfo.rol,
            userBasicInfo.sexo);
        final remoteUserData = await remoteDataSource.addUserBasicInfo(modelo);
        localDataSource.cacheBasicUserData(modelo);
        return Right(remoteUserData);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }
}
