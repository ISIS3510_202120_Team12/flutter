import 'package:equatable/equatable.dart';

/**
 * this class represents the user basic data, this will be mainly used to crate the from when registing the data
 *
 */

class UserBasicInfo extends Equatable {
  //this constants were defined to understand the definen values for de dataBase
  static const String DEPTO_QUIRURGICA = "quirurgica";
  static const String DEPTO_PEDIATRICO = "pediatrico";
  static const String DEPTO_CARDIOPATIA_CONGENITA = "cardiopatia_congenita";
  static const String DEPTO_CARDIOVASCULAR = "cardiovascular";
  static const String DEPTO_NEUROCIENCIAS = "neurociencias";
  static const String DEPTO_ORTOPEDIA = "ortopedia";
  static const String DEPTO_TRANSPLANTES = "transplantes";
  static const int MEDICO = 1;
  static const int PACIENTE = 2;
  static const int ADMINISTRADOR = 3;
  final String deptoCardio;
  final bool empleado;
  final DateTime fechaNacimiento;
  final String nombreCompleto;
  final int rol;
  final String sexo;

  const UserBasicInfo(this.deptoCardio, this.empleado, this.fechaNacimiento,
      this.nombreCompleto, this.rol, this.sexo);

  @override
  // TODO: implement props
  List<Object?> get props => [deptoCardio, empleado, nombreCompleto, rol, sexo];
}
