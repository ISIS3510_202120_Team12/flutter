import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/features/user_basic_data/domain/entities/user_basic_info.dart';
import 'package:dartz/dartz.dart';

/**
 * this class is a contract that must be implemented in the data layer
 * it handles what methods must be implemented in order to let the
 * app work
 */
abstract class UserBasicInfoRepository {
  Future<Either<Failure, UserBasicInfo>> getUserBasicInfo();

  Future<Either<Failure, UserBasicInfo>> setUserData(
      UserBasicInfo userBasicInfo);
}
