import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/usecases/usecase.dart';
import 'package:corazon_saludable/features/user_basic_data/domain/entities/user_basic_info.dart';
import 'package:corazon_saludable/features/user_basic_data/domain/repositories/user_basic_info_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class GetUserBasicInfo implements UseCase<UserBasicInfo, NoParams> {
  final UserBasicInfoRepository repository;

  GetUserBasicInfo(this.repository);

  @override
  Future<Either<Failure, UserBasicInfo>> call(NoParams params) async {
    return await repository.getUserBasicInfo();
  }
}

