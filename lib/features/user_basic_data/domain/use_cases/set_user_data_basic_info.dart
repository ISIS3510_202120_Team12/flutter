import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/usecases/usecase.dart';
import 'package:corazon_saludable/features/user_basic_data/domain/entities/user_basic_info.dart';
import 'package:corazon_saludable/features/user_basic_data/domain/repositories/user_basic_info_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class SetUserBasicInfo implements UseCase<UserBasicInfo, Params> {
  final UserBasicInfoRepository repository;

  SetUserBasicInfo(this.repository);

  @override
  Future<Either<Failure, UserBasicInfo>> call(Params params) {
    return repository.setUserData(params.toSave);
  }
}

class Params extends Equatable {
  final UserBasicInfo toSave;

  //TODO: revisar la integridad de esta parte del código
  const Params(this.toSave);

  @override
  List<Object> get props => [toSave];
}
