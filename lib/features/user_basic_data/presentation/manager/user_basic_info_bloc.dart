import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/usecases/usecase.dart';
import 'package:corazon_saludable/features/user_basic_data/domain/entities/user_basic_info.dart';
import 'package:corazon_saludable/features/user_basic_data/domain/use_cases/get_user_basic_info.dart';
import 'package:corazon_saludable/features/user_basic_data/domain/use_cases/set_user_data_basic_info.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'user_basic_info_event.dart';
part 'user_basic_info_state.dart';

const String SERVER_FAILURE_MESSAGE = 'Actualmente no cuentas con conexión a internet.';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';
class UserBasicInfoBloc extends Bloc<UserBasicInfoEvent, UserBasicInfoState> {
  final GetUserBasicInfo getUserBasicInfo;
  final SetUserBasicInfo setUserBasicInfo;
  UserBasicInfoBloc(
      {required GetUserBasicInfo infoBasica,
       required SetUserBasicInfo setInfoBasica,})
      : assert(infoBasica != null),
        assert(setInfoBasica != null),
        getUserBasicInfo = infoBasica,
        setUserBasicInfo = setInfoBasica, super(Empty());

  /// Rodrigo this method maps each event thrown from the UI to an specific state
  @override
  Stream<UserBasicInfoState> mapEventToState(
      UserBasicInfoEvent event,
      ) async* {
    if(event is GetUserBasicInfoEvent){
      yield Loading();
      final failureOrUserBasicInfo = await getUserBasicInfo(NoParams());
      yield* _eitherLoadedOrErrorStateGet(failureOrUserBasicInfo);
    } if(event is SetUserDataEvent)
      {
        yield Loading();
        final failureOrUserBasicInfo = await setUserBasicInfo(Params(event.userBasicInfo));
        yield* _eitherLoadedOrErrorStateSend(failureOrUserBasicInfo);
      }
  }

  /// Rodrigo: this method handles de functional programming that we used
  /// with the library 'dartz'. By the implemented standard, the left side
  /// @returns a failure when there is any kind of error, while the right one
  /// @returns an entity of UserBasicInfo type that contains the user data
  /// the final @return of this method is a new state for this Bloc.
  Stream<UserBasicInfoState> _eitherLoadedOrErrorStateGet(
      Either<Failure, UserBasicInfo> gottenUser
      ) async*{
    yield gottenUser.fold(
            (failure) => Error(_mapFailureToMessage(failure)),
            (userBasicInfo) => Loaded(userBasicInfo),);
  }
  /// Rodrigo: this method behaves similar to _eitherLoadedOrErrorStateGet but it
  /// uses a different state that represents that the data was sent to the
  /// firabase storage
  Stream<UserBasicInfoState> _eitherLoadedOrErrorStateSend(
      Either<Failure, UserBasicInfo> gottenUser
      ) async*{
    yield gottenUser.fold(
          (failure) => Error(_mapFailureToMessage(failure)),
          (userBasicInfo) => Sent(userBasicInfo),);
  }
  /// Rodrigo: this method maps the different kinds of error to it's specific
  /// messages

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }

}



