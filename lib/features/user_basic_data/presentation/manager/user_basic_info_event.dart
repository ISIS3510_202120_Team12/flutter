part of 'user_basic_info_bloc.dart';

@immutable
abstract class UserBasicInfoEvent extends Equatable{
  @override
  List<Object> get props => [];
}
class GetUserBasicInfoEvent extends UserBasicInfoEvent{}

class SetUserDataEvent extends UserBasicInfoEvent{
  final UserBasicInfo userBasicInfo;

  SetUserDataEvent(this.userBasicInfo);

  @override
  List<Object> get props => [userBasicInfo];

}
