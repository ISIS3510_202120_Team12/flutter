part of 'user_basic_info_bloc.dart';

@immutable
abstract class UserBasicInfoState extends Equatable {
  @override
  List<Object> get props => [];
}

class UserBasicInfoInitial extends UserBasicInfoState {}

// no state at all
class Empty extends UserBasicInfoState {}

// the app is trying to load an object
class Loading extends UserBasicInfoState {}

// the application performed the action
class Loaded extends UserBasicInfoState {
  final UserBasicInfo userBasicInfo;

  Loaded(this.userBasicInfo);

  @override
  List<Object> get props => [userBasicInfo];
}

// the aplication had an error
class Error extends UserBasicInfoState {
  final String message;

  Error(this.message);

  @override
  List<Object> get props => [message];
}
class Sent extends UserBasicInfoState{
  final UserBasicInfo userBasicInfo;

  Sent(this.userBasicInfo);

}
