import 'package:corazon_saludable/UI/main_menu.dart';
import 'package:corazon_saludable/features/user_basic_data/presentation/manager/user_basic_info_bloc.dart';
import 'package:corazon_saludable/features/user_basic_data/presentation/widgets/display_message_widget.dart';
import 'package:corazon_saludable/features/user_basic_data/presentation/widgets/loading_widget.dart';
import 'package:corazon_saludable/features/user_basic_data/presentation/widgets/user_basic_data_form.dart';
import 'package:corazon_saludable/injection_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserBasicDataPage extends StatelessWidget {
  bool iniciarLleno;
  UserBasicDataPage( {Key? key, bool iniciar = false }): iniciarLleno = iniciar,  super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) =>  MainMenu())),
          ),
          title: const Text("Ingresa Tus Datos"),
          backgroundColor: const Color(0xff002d72),
        ),
        body: SingleChildScrollView(
          child: buildBody(context),
        ));
  }

  BlocProvider<UserBasicInfoBloc> buildBody(BuildContext context) {
    return BlocProvider(
      create: (_) => sl<UserBasicInfoBloc>(),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(3),
          child: Column(
            children: <Widget>[
              const SizedBox(height: 1),
              BlocBuilder<UserBasicInfoBloc,UserBasicInfoState>(
                  builder: (context, state){
                    if(state is Empty){
                      if(iniciarLleno){
                        BlocProvider.of<UserBasicInfoBloc>(context).add(GetUserBasicInfoEvent());
                        print("inició en true");
                      }
                      return UserBasicDataForm();
                    }
                    else if(state is Loading){
                      print("LOADING");
                      return const LoadingWidget();
                    }
                    else if(state is Loaded){
                      print("LOADED");
                      return UserBasicDataForm(valorPorDefecto: state.userBasicInfo);
                    }
                    else if(state is Error){
                      return MessageDisplay(message: state.message);
                    }
                    else if(state is Sent) {
                      return UserBasicDataForm(valorPorDefecto: state.userBasicInfo);
                    }
                    else{
                      return MessageDisplay(message: "Estado indefinido");
                    }
                  })
            ],
          ),
        ),
      ),
    );
  }
}

