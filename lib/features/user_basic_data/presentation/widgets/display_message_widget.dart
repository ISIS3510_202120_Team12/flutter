import 'package:corazon_saludable/UI/main_menu.dart';
import 'package:flutter/material.dart';

class MessageDisplay extends StatelessWidget {
  final String? message;

  const MessageDisplay({
    Key ?key,
    @required this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 3,
      child: Column(
        children: <Widget>[Center(
          child: SingleChildScrollView(
            child: Text(
              message!,
              style: TextStyle(fontSize: 25),
              textAlign: TextAlign.center,
            ),
          ),
        ), MaterialButton(onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => MainMenu()));
        }, child: Text("Regresar al menú principal"),)],
      ),
    );
  }
}
