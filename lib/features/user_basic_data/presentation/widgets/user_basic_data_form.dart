//import 'package:flutter/cupertino.dart';
import 'package:corazon_saludable/core/firebase/firebase_analytics.dart';
import 'package:corazon_saludable/features/user_basic_data/domain/entities/user_basic_info.dart';
import 'package:corazon_saludable/features/user_basic_data/presentation/manager/user_basic_info_bloc.dart';
import 'package:corazon_saludable/injection_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:intl/intl.dart';
class UserBasicDataForm extends StatelessWidget {
  final _formKey = GlobalKey<FormBuilderState>();
  UserBasicInfo usuarioActual;


  UserBasicDataForm({Key? key, UserBasicInfo? valorPorDefecto}) :
        usuarioActual = valorPorDefecto  ?? UserBasicInfo("", false,  DateTime.now(), "", 0, "") ,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height / 1.1,
      child: Scaffold(
        body: FormBuilder(
          key: _formKey,
          child: SingleChildScrollView(
            child : Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
            children: [
              const SizedBox(height: 35),
              FormBuilderTextField(
                name: "nombre",
                decoration: const InputDecoration(
                    labelText: "Ingresa tu nombre completo",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    filled: true,
                    hintStyle: TextStyle(color: Color(0xFF424242)),
                    hintText: "Inserte su nombre completo",
                    fillColor: Colors.white70),
                validator: FormBuilderValidators.required(context),
              ),
              FormBuilderDateTimePicker(
                name: "fechaNacimiento",
                inputType: InputType.date,
                format: DateFormat("dd-MM-yyyy"),
                decoration: const InputDecoration(labelText: "Seleccione su fecha de nacimiento"),
                //initialValue: DateTime.now(),
                validator: FormBuilderValidators.required(context),
              ),
              FormBuilderChoiceChip(
                  name: 'sexo',
                  validator: FormBuilderValidators.required(context),
                  decoration: const InputDecoration(
                    labelText: "Selecciona tu sexo"
                  ),
                  options:const [
                    FormBuilderFieldOption(value: "Masculino",
                    child: Text("Masculino 🚹"), ),
                    FormBuilderFieldOption(value: "Femenino",
                      child: Text("Femenino  🚺"),),
                    FormBuilderFieldOption(value: "No especifico",
                      child: Text("No Específico ⚧"),),
                  ] ),
              FormBuilderCheckbox(
                  name: "empleado",
                  title: const Text("Seleccione si usted es empleado de la Cardio")),
              FormBuilderDropdown(
                  name: "rol",
                  validator: FormBuilderValidators.required(context),
                  decoration: const InputDecoration(
                      labelText: 'rol'
                  ),
                  hint: const Text("Selecciona tu rol"),
                  items: const [
                    DropdownMenuItem(
                        value: 1,
                        child: Text("Personal Médico")),
                    DropdownMenuItem(
                        value: 2,
                        child: Text("Paciente")),
                    DropdownMenuItem(
                        value: 3,
                        child: Text("Administrativo"))
                  ]),
              FormBuilderChoiceChip(
                  name: 'departamento',
                  decoration: const InputDecoration(
                      labelText: "Selecciona el departamento de la cardio con el que estas asociado"
                  ),
                  options:const [
                    FormBuilderFieldOption(value: "QUIRURGICA",
                      child: Text("Quirurgica"), ),
                    FormBuilderFieldOption(value: "PEDIATRICO",
                      child: Text("Pedriatico "),),
                    FormBuilderFieldOption(value: "CARDIOPATIA_CONGENITA",
                      child: Text("Cardiopatía Congénita"),),
                    FormBuilderFieldOption(value: "CARDIOVASCULAR",
                      child: Text("cardiovascular"), ),
                    FormBuilderFieldOption(value: "NEUROCIENCIAS",
                      child: Text("Neurociencias "),),
                    FormBuilderFieldOption(value: "ORTOPEDIA",
                      child: Text("Ortopedia"),),
                    FormBuilderFieldOption(value: "TRASPLANTES",
                      child: Text("Trasplantes"),),
                    FormBuilderFieldOption(value: "ADMINISTRACION",
                      child: Text("Administración"),),
                  ] ,
                validator: FormBuilderValidators.required(context),),
              //!revisar el botón con gradiente
              const SizedBox(height: 30),
              ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: Stack(
                  children: <Widget>[
                    Positioned.fill(
                      child: Container(
                        decoration: const BoxDecoration(
                          gradient: LinearGradient(
                            colors: <Color>[
                              Color(0xFF0D47A1),
                              Color(0xFF1976D2),
                              Color(0xff002d72),
                            ],
                          ),
                        ),
                      ),
                    ),
                    TextButton(
                      style: TextButton.styleFrom(
                        padding: const EdgeInsets.all(16.0),
                        primary: Colors.white,
                        textStyle: const TextStyle(fontSize: 20),
                      ),
                      onPressed: () {
                        final validationSucess = _formKey.currentState!.validate();
                        if(validationSucess)
                          {
                        //! the current form data is saved (only in the form context)
                        _formKey.currentState?.save();
                        //! the data is extracted
                        final formData = _formKey.currentState?.value;
                        sl<AnalyticService>().dataUpdated(nombre: formData!["nombre"]);
                        var cosita = UserBasicInfo(formData!["departamento"],
                          formData["empleado"],
                          formData["fechaNacimiento"],
                          formData["nombre"],
                          formData["rol"] as int,
                          formData["sexo"],);
                        BlocProvider.of<UserBasicInfoBloc>(context).add(SetUserDataEvent(cosita));
                        //! finally the data is shown
                        FocusScope.of(context).unfocus();
                       }
                      },
                      child: const Text('Guardar'),
                    ),
                  ],
                ),
              ),
            const SizedBox(height: 40,)],
          ),
            ),),
          autovalidateMode: AutovalidateMode.onUserInteraction,
          //! this initial value works as a dictionary that uses each field key
          //! to give them an initial value {"nombre": usuarioActual.nombreCompleto }
          initialValue: _getInitialValueDictionaty(usuarioActual) ,
        ),
      ),
    );
  }
  //! Rodrigo: this method is used to create 
  _getInitialValueDictionaty(UserBasicInfo user){
    Map<String, Object> datos= {"nombre":"",
    };

    print("Adentro");
    print(user);
    print(usuarioActual);
    if(user.nombreCompleto == "" ){
      return datos;
    }
    else{ datos ={"departamento":user.deptoCardio,
    "empleado" : user.empleado,
    "fechaNacimiento":user.fechaNacimiento,
    "nombre": user.nombreCompleto,
    "rol": user.rol,
    "sexo": user.sexo};
     return datos;
    }
  }
}

