
import 'package:equatable/equatable.dart';

/**
 * this class represents the user medical info.
 *
 */
class UserInfoMed extends Equatable {
  final double altura;
  final int colesterolTotal;
  final bool consumoAlcohol;
  final bool diabetico;
  final bool fumador;
  final int peso;
  final int presionSistolica;
  final int spo2;

  UserInfoMed(
      this.altura,
      this.colesterolTotal,
      this.consumoAlcohol,
      this.diabetico,
      this.fumador,
      this.peso,
      this.presionSistolica,
      this.spo2);

  @override
// TODO: implement props
  List<Object?> get props => [
    altura,
    colesterolTotal,
    consumoAlcohol,
    diabetico,
    fumador,
    peso,
    presionSistolica,
    spo2
  ];
}
