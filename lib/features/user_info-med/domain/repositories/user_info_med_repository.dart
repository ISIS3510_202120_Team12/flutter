import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/features/user_info-med/domain/entities/user_info_med.dart';
import 'package:dartz/dartz.dart';

/**
 * this class is a contract that must be implemented in the data layer
 * it handles what methods must be implemented in order to let the
 * app work
 */
abstract class UserInfoMedRepository {
  Future<Either<Failure, UserInfoMed>> getUserInfoMed();

  Future<Either<Failure, UserInfoMed>> setUserInfoMed(UserInfoMed);

  Future<Either<Failure, UserInfoMed>> putUserInfoMed(UserInfoMed);
}
