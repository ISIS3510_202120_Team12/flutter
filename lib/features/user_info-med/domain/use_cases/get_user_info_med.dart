

import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/usecases/usecase.dart';
import 'package:corazon_saludable/features/user_info-med/domain/entities/user_info_med.dart';
import 'package:corazon_saludable/features/user_info-med/domain/repositories/user_info_med_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class getUserInfoMed implements UseCase<UserInfoMed, Params>
{
  final UserInfoMedRepository repository;

  getUserInfoMed(this.repository);

  @override
  Future<Either<Failure, UserInfoMed>> call(Params params) async {
    return await repository.getUserInfoMed();
  }

}

class Params extends Equatable {
  @override
  List<Object> get props => [];
}