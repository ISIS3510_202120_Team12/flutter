
import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/usecases/usecase.dart';
import 'package:corazon_saludable/features/user_info-med/domain/entities/user_info_med.dart';
import 'package:corazon_saludable/features/user_info-med/domain/repositories/user_info_med_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class PutUserInfoMed implements UseCase<UserInfoMed, Params> {
  final UserInfoMedRepository repository;

  PutUserInfoMed(this.repository);

  @override
  Future<Either<Failure, UserInfoMed>> call(Params params) async {
    return repository.putUserInfoMed(params.toSave);
  }
}

class Params extends Equatable {
  final UserInfoMed toSave;

  //TODO: revisar la integridad de esta parte del código
  const Params(this.toSave);

  @override
  List<Object> get props => [toSave];
}
