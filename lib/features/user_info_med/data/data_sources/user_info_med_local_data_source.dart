import 'dart:convert';
import 'package:corazon_saludable/core/error/exeptions.dart';
import 'package:corazon_saludable/features/user_info_med/data/models/user_info_med_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class UserInfoMedLocalDataSource{
  /// Gets the cached [UserBasicInfoModel] which was gotten the last time
  /// the user had an internet connection.
  ///
  /// Throws [CacheException] if no cached data is present.
  Future<UserInfoMedModel> getLastBasicUserData();

  Future<void> cacheUserInfoMed (UserInfoMedModel userDataToCache);
}

const CACHED_USER_BASIC_DATA = 'CACHED_USER_BASIC_DATA';

class UserInfoMedLocalDataSourceImpl implements UserInfoMedLocalDataSource {
  final SharedPreferences sharedPreferences;

  UserInfoMedLocalDataSourceImpl({required SharedPreferences preferences})
      : sharedPreferences = preferences;

  @override
  Future<void> cacheInfoMedData(UserInfoMedModel userInfoMedCache) {
    //!TODO this method will have to print a dictionary coded as a json String.

    return sharedPreferences.setString(
        CACHED_USER_BASIC_DATA, json.encode(userInfoMedCache.toJson()));
  }

  @override
  Future<UserInfoMedModel> getLastUserInfoMedData() {
    final jsonString = sharedPreferences.getString(CACHED_USER_BASIC_DATA);
    if (jsonString != null) {
      return Future.value(UserInfoMedModel.fromJson(json.decode(jsonString)));
    } else {
      throw CacheException();
    }
  }
  @override
  noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
}
