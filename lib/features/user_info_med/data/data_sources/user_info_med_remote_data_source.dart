import 'dart:convert';
import 'package:corazon_saludable/core/error/exeptions.dart';
import 'package:corazon_saludable/features/user_info_med/data/models/user_info_med_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;

abstract class UserInfoMedRemoteDataSource {
  /// Calls the https://us-central1-backendcorazonsaludable.cloudfunctions.net/user/{number} endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<UserInfoMedModel> getUserInfoMed();

  /// Posts new user info https://us-central1-backendcorazonsaludable.cloudfunctions.net/user/{number}endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<UserInfoMedModel> addUserInfoMed(UserInfoMedModel modelo);
}

class UserInfoMedRemoteDataSourceImpl implements UserInfoMedRemoteDataSource {
  final http.Client client;
  String? logueado = FirebaseAuth.instance.currentUser?.uid;
  late String apiUrl =
      "https://us-central1-backendcorazonsaludable.cloudfunctions.net/user/$logueado";
  //Rodrigo:  newer versions of the http library require use Uri data type instead
  //of the a string with
  late Uri uriApi = Uri.parse(apiUrl);
  UserInfoMedRemoteDataSourceImpl(this.client);

  @override
  Future<UserInfoMedModel> addUserInfoMed(UserInfoMedModel modelo) async {
    //TODO: Eliminar los prints de debugs del código final
    final response = await client.put(uriApi,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        //body: json.encode(modelo.toJson())
        );
    //TODO: Eliminar los prints de debugs del código final
   // print("terminé de hacer el put, voy a verificar mi código");
    if (response.statusCode == 201) {
      return modelo;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<UserInfoMedModel> getUserInfoMed() async {
    final response = await client.get(
      uriApi,
      headers: {
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 200) {
      return UserInfoMedModel.fromJson(json.decode(response.body)['body']);
    } else {
      throw ServerException();
    }
  }
}
