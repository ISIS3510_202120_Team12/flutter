import 'package:cloud_firestore_platform_interface/src/timestamp.dart';
import 'package:corazon_saludable/features/user_info_med/domain/entities/user_info_med.dart';
import 'package:flutter/physics.dart';
import 'package:meta/meta.dart';
import 'dart:developer';
import 'package:intl/intl.dart';

class UserInfoMedModel extends UserInfoMed {
  UserInfoMedModel(double altura, double colesterol, bool consumoAlcohol,
      bool diabetico, bool fumador, double peso, double presionSistolica)
      : super(altura, colesterol, consumoAlcohol, diabetico, fumador, peso,
            presionSistolica);
  factory UserInfoMedModel.fromJson(Map<String, dynamic> json) {
    return UserInfoMedModel(json['altura'] as double, json['colesterol'] as double, json['consumoAlcohol'] as bool,
        json['diabetico'] as bool, json['fumador'] as bool, json['peso'], json['presionSistolica']);
  }
  Map<String, dynamic> toJson(){
    return{
      "altura":altura,
      "colesterol":colesterol,
      "consumoAlcohol":consumoAlcohol,
      "diabetico":diabetico,
      "fumador":fumador,
      "peso":peso,
      "presionSistolica":presionSistolica
    };
  }
}