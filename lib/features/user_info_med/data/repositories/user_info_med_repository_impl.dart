import 'package:corazon_saludable/core/error/exeptions.dart';
import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/network/network_info.dart';
import 'package:corazon_saludable/features/user_info_med/data/data_sources/user_info_med_local_data_source.dart';
import 'package:corazon_saludable/features/user_info_med/data/data_sources/user_info_med_remote_data_source.dart';
import 'package:corazon_saludable/features/user_info_med/data/models/user_info_med_model.dart';
import 'package:corazon_saludable/features/user_info_med/domain/entities/user_info_med.dart';
import 'package:corazon_saludable/features/user_info_med/domain/repositories/user_info_med_repository.dart';
import 'package:dartz/dartz.dart';

class UserInfoMedRepositoryImpl implements UserInfoMedRepository {
  //reference of the local data source
  final UserInfoMedLocalDataSource localDataSource;

//reference of the remote data source
  final UserInfoMedRemoteDataSource remoteDataSource;

//reference to network info, service that checks is the device is onLine
  final NetworkInfo networkInfo;

  UserInfoMedRepositoryImpl(
      this.localDataSource, this.remoteDataSource, this.networkInfo);
  @override
  Future<Either<Failure, UserInfoMed>> getUserInfoMed() async {
    if (await networkInfo.isConnected) {
      try {
        UserInfoMedModel remoteUserData = await remoteDataSource.getUserInfoMed();
        localDataSource.cacheUserInfoMed(remoteUserData);
        return Right(remoteUserData);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localUserData = await localDataSource.getLastBasicUserData();
        return Right(localUserData);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }
  /// Rodrigo: this method uploads the user basic data of the used into the cloud
  /// also saves a local copy in the shared preferences
  //TODO: correct this monstrosity against the humanity
  @override
  Future<Either<Failure, UserInfoMed>> setUserInfoMed(userInfoMed) async {
    if (await networkInfo.isConnected) {
      try {
        final modelo = UserInfoMedModel(
            userInfoMed.altura,
            userInfoMed.colesterol,
            userInfoMed.consumoAlcohol,
            userInfoMed.diabetico,
            userInfoMed.fumador,
            userInfoMed.peso,
            userInfoMed.presionSistolica);
        final remoteUserInfoMed = await remoteDataSource.addUserInfoMed(modelo);
        localDataSource.cacheUserInfoMed(modelo);
        return Right(remoteUserInfoMed);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }
}