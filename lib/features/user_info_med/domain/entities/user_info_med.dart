import 'package:equatable/equatable.dart';

class UserInfoMed extends Equatable {
  final double altura;
  final double colesterol;
  final bool consumoAlcohol;
  final bool diabetico;
  final bool fumador;
  final double peso;
  final double presionSistolica;

  const UserInfoMed(this.altura, this.colesterol, this.consumoAlcohol, this.diabetico, this.fumador, this.peso,
      this.presionSistolica);
  @override
  // TODO: implement props
  List<Object?> get props => [altura, colesterol, consumoAlcohol, diabetico, fumador, peso,
    presionSistolica];
}