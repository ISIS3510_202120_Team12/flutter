import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/features/user_info_med/domain/entities/user_info_med.dart';
import 'package:dartz/dartz.dart';


abstract class UserInfoMedRepository {
  Future<Either<Failure, UserInfoMed>> getUserInfoMed();

  Future<Either<Failure, UserInfoMed>> setUserInfoMed(
      UserInfoMed userInfoMed);
}