import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/usecases/usecase.dart';
import 'package:corazon_saludable/features/user_info_med/domain/entities/user_info_med.dart';
import 'package:corazon_saludable/features/user_info_med/domain/repositories/user_info_med_repository.dart';
import 'package:dartz/dartz.dart';

class GetUserInfoMed implements UseCase<UserInfoMed, NoParams> {
  final UserInfoMedRepository repository;

  GetUserInfoMed(this.repository);

  @override
  Future<Either<Failure, UserInfoMed>> call(NoParams params) async {
    return await repository.getUserInfoMed();
  }
}
