import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/usecases/usecase.dart';
import 'package:corazon_saludable/features/user_info_med/domain/entities/user_info_med.dart';
import 'package:corazon_saludable/features/user_info_med/domain/repositories/user_info_med_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class SetUserInfoMed implements UseCase<UserInfoMed, Params> {
  final UserInfoMedRepository repository;

  SetUserInfoMed(this.repository);

  @override
  Future<Either<Failure, UserInfoMed>> call(Params params) {
    return repository.setUserInfoMed(params.toSave);
  }
}

class Params extends Equatable {
  final UserInfoMed toSave;

  //TODO: revisar la integridad de esta parte del código
  const Params(this.toSave);

  @override
  List<Object> get props => [toSave];
}
