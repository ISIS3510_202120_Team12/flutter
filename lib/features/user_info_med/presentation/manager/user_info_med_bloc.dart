import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/usecases/usecase.dart';
import 'package:corazon_saludable/features/user_info_med/domain/entities/user_info_med.dart';
import 'package:corazon_saludable/features/user_info_med/domain/use_cases/get_user_info_med.dart';
import 'package:corazon_saludable/features/user_info_med/domain/use_cases/set_user_info_med.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'user_info_med_event.dart';
part 'user_info_med_state.dart';

const String SERVER_FAILURE_MESSAGE = 'Actualmente no cuentas con conexión a internet.';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';
class UserInfoMedBloc extends Bloc<UserInfoMedEvent, UserInfoMedState> {
  final GetUserInfoMed getUserInfoMed;
  final SetUserInfoMed setUserInfoMed;
  UserInfoMedBloc(
      {required GetUserInfoMed infoMed,
       required SetUserInfoMed setInfoMed,})
      : assert(infoMed != null),
        assert(setInfoMed != null),
        getUserInfoMed = infoMed,
        setUserInfoMed = setInfoMed, super(Empty());

  /// Rodrigo this method maps each event thrown from the UI to an specific state
  @override
  Stream<UserInfoMedState> mapEventToState(
      UserInfoMedEvent event,
      ) async* {
    if(event is GetUserInfoMedEvent){
      yield Loading();
      final failureOrUserInfoMed = await getUserInfoMed(NoParams());
      yield* _eitherLoadedOrErrorStateGet(failureOrUserInfoMed);
    } if(event is SetUserInfoMedEvent)
      {
        yield Loading();
        final failureOrUserInfoMed = await setUserInfoMed(Params(event.userInfoMed));
        yield* _eitherLoadedOrErrorStateSend(failureOrUserInfoMed);
      }
  }
    Stream<UserInfoMedState> _eitherLoadedOrErrorStateGet(
      Either<Failure, UserInfoMed> gottenUser
      ) async*{
    yield gottenUser.fold(
            (failure) => Error(_mapFailureToMessage(failure)),
            (userInfoMed) => Loaded(userInfoMed),);
  }

  Stream<UserInfoMedState> _eitherLoadedOrErrorStateSend(
      Either<Failure, UserInfoMed> gottenUser
      ) async*{
    yield gottenUser.fold(
          (failure) => Error(_mapFailureToMessage(failure)),
          (userInfoMed) => Sent(userInfoMed),);
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }

}



