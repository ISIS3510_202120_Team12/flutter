part of 'user_info_med_bloc.dart';

@immutable
abstract class UserInfoMedEvent extends Equatable{
  @override
  List<Object> get props => [];
}
class GetUserInfoMedEvent extends UserInfoMedEvent{}

class SetUserInfoMedEvent extends UserInfoMedEvent{
  final UserInfoMed userInfoMed;

  SetUserInfoMedEvent(this.userInfoMed);

  @override
  List<Object> get props => [userInfoMed];

}
