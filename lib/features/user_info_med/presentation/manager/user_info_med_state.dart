part of 'user_info_med_bloc.dart';

@immutable
abstract class UserInfoMedState extends Equatable {
  @override
  List<Object> get props => [];
}

class UserBasicInfoInitial extends UserInfoMedState {}

// no state at all
class Empty extends UserInfoMedState {}

// the app is trying to load an object
class Loading extends UserInfoMedState {}

// the application performed the action
class Loaded extends UserInfoMedState {
  final UserInfoMed userInfoMed;

  Loaded(this.userInfoMed);

  @override
  List<Object> get props => [userInfoMed];
}

// the aplication had an error
class Error extends UserInfoMedState {
  final String message;

  Error(this.message);

  @override
  List<Object> get props => [message];
}
class Sent extends UserInfoMedState{
  final UserInfoMed userInfoMed;

  Sent(this.userInfoMed);

}
