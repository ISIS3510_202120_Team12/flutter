import 'package:corazon_saludable/UI/main_menu.dart';
import 'package:corazon_saludable/features/user_info_med/presentation/manager/user_info_med_bloc.dart';
import 'package:corazon_saludable/features/user_info_med/presentation/widgets/display_message_widget_info_med.dart';
import 'package:corazon_saludable/features/user_info_med/presentation/widgets/loading_widget_info_med.dart';
import 'package:corazon_saludable/features/user_info_med/presentation/widgets/user_info_med_form.dart';
import 'package:corazon_saludable/injection_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserInfoMedPage extends StatelessWidget {
  bool iniciarLleno;
  UserInfoMedPage({Key? key, bool iniciar = false})
      : iniciarLleno = iniciar,
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.push(
                context, MaterialPageRoute(builder: (context) => MainMenu())),
          ),
          title: const Text("Información Médica"),
          backgroundColor: const Color(0xff002d72),
        ),
        body: SingleChildScrollView(
            //child: buildBody(context),
            ));
  }

  BlocProvider<UserInfoMedBloc> buildBody(BuildContext context) {
    return BlocProvider(
      create: (_) => sl<UserInfoMedBloc>(),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(3),
          child: Column(
            children: <Widget>[
              const SizedBox(height: 1),
              BlocBuilder<UserInfoMedBloc, UserInfoMedState>(
                  builder: (context, state) {
                if (state is Empty) {
                  if (iniciarLleno) {
                    BlocProvider.of<UserInfoMedBloc>(context)
                        .add(GetUserInfoMedEvent());
                    print("inició en true");
                  }
                  return UserInfoMedForm();
                } else if (state is Loading) {
                  return const LoadingWidget();
                } else if (state is Error) {
                  return MessageDisplay(message: state.message);
                }else{
                  return MessageDisplay(message: "Estado indefinido");
                }
              })
            ],
          ),
        ),
      ),
    );
  }
}
