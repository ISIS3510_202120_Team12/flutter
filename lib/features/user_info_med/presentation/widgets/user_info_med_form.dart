//import 'package:flutter/cupertino.dart';
import 'package:corazon_saludable/features/user_info_med/domain/entities/user_info_med.dart';
import 'package:corazon_saludable/features/user_info_med/presentation/manager/user_info_med_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class UserInfoMedForm extends StatelessWidget {
  final _formKey = GlobalKey<FormBuilderState>();
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height / 1.1,
      child: Scaffold(
        body: FormBuilder(
          key: _formKey,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  const SizedBox(height: 35),
                  Text("Nombre", style: TextStyle(color: Color(0xFF424242))),
                  Text("Altura", style: TextStyle(color: Color(0xFF424242))),
                  Text("Colesterol",
                      style: TextStyle(color: Color(0xFF424242))),
                  Text("Alcohol", style: TextStyle(color: Color(0xFF424242))),
                  FormBuilderChoiceChip(name: 'Alcohol',
                      //child: Text("Colesterol"),
                      options: const [
                        FormBuilderFieldOption(
                          value: "Sí",
                          child: Text("Sí"),
                        ),
                        FormBuilderFieldOption(
                          value: "No",
                          child: Text("No"),
                        ),
                      ]),
                  FormBuilderChoiceChip(name: 'Diabetes',
                      //child: Text("Colesterol"),
                      options: const [
                        FormBuilderFieldOption(
                          value: "Sí",
                          child: Text("Sí"),
                        ),
                        FormBuilderFieldOption(
                          value: "No",
                          child: Text("No"),
                        ),
                      ]),
                  FormBuilderChoiceChip(name: 'Fumador',
                      //child: Text("Fumador"),
                      options: const [
                        FormBuilderFieldOption(
                          value: "Sí",
                          child: Text("Sí"),
                        ),
                        FormBuilderFieldOption(
                          value: "No",
                          child: Text("No"),
                        ),
                      ]),
                  const SizedBox(height: 30),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
