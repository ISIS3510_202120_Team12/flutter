import 'dart:convert';

import 'package:corazon_saludable/core/error/exeptions.dart';
import 'package:corazon_saludable/features/user_preferences/data/models/user_preferences_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class UserPreferencesLocalDataSource {
  /// Gets the cached [UserPreferencesModelModel] which was gotten the last time
  /// the user had an internet connection.
  ///
  /// Throws [CacheException] if no cached data is present.
  Future<UserPreferencesModel> getLastUserPreference();

  Future<void> cacheUserPreference(UserPreferencesModel userPreferenceToCache);
}
const CACHED_USER_PREFERENCE = 'CACHED_USER_PREFERENCE';

class UserPreferenceLocalDataSourceImpl implements UserPreferencesLocalDataSource{
  final SharedPreferences sharedPreferences;

  UserPreferenceLocalDataSourceImpl({required SharedPreferences preferences})
      : sharedPreferences = preferences;

  @override
  Future<void> cacheUserPreference(UserPreferencesModel userPreferenceToCache) {

    return sharedPreferences.setString(
        CACHED_USER_PREFERENCE,
        json.encode(userPreferenceToCache.toJson()
        )
    );
  }

  @override
  Future<UserPreferencesModel> getLastUserPreference() {
    print("AAAAAAAAAAAAA");
    final jsonString = sharedPreferences.getString(CACHED_USER_PREFERENCE);
    if(jsonString != null){
      return Future.value(UserPreferencesModel.fromJson(json.decode(jsonString)));
    } else {
      throw CacheException();
    }
  }

}
