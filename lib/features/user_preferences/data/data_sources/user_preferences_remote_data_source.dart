import 'dart:convert';
import 'package:corazon_saludable/core/error/exeptions.dart';
import 'package:corazon_saludable/features/user_preferences/data/models/user_preferences_model.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:http/http.dart' as http;

abstract class UserPreferencesRemoteDataSource{
  /// Calls the https://us-central1-backendcorazonsaludable.cloudfunctions.net/user/{number} endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<UserPreferencesModel> getUserPreferences();

  /// Posts new user info https://us-central1-backendcorazonsaludable.cloudfunctions.net/user/{number}endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<UserPreferencesModel> addUserPreference(UserPreferencesModel modelo);

}


class UserPreferenceRemoteDataSourceImpl implements UserPreferencesRemoteDataSource{
  final http.Client client;
  //Comment next line for test to work
  String? logueado =  FirebaseAuth.instance.currentUser?.uid;
  late String apiUrl = "https://us-central1-backendcorazonsaludable.cloudfunctions.net/preferencias/gsEn12OBphuhNTd33ldw";
  late Uri uriApi = Uri.parse(apiUrl);
  UserPreferenceRemoteDataSourceImpl(this.client);

  @override
  Future<UserPreferencesModel> addUserPreference(UserPreferencesModel modelo) async
  {
    final response  = await client.put(uriApi,headers: <String, String>
    {
      'Content-Type': 'application/json; charset=UTF-8',
    }, body: json.encode(modelo.toJson()) );
    if(response.statusCode == 201){
      return modelo;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<UserPreferencesModel> getUserPreferences() async {
    print(uriApi);
    final response = await client.get(uriApi,
      headers: {
        'Content-Type': 'application/json',
      },);
    if (response.statusCode ==200){
      print(logueado);
      print(response.body);
      return UserPreferencesModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}
