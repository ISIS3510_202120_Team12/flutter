import 'package:corazon_saludable/features/user_preferences/domain/entities/user_preferences.dart';

class UserPreferencesModel extends UserPreferences
{
  UserPreferencesModel(
      List <bool> alergias,
      String ubicacionHogar,
      bool celiaco,
      List  <String> deportes,
      bool vegano,
      bool vegetariano
      )
      : super( alergias,ubicacionHogar , celiaco,deportes ,vegano , vegetariano );

  factory UserPreferencesModel.fromJson(Map<String, dynamic> json)
  {
    return UserPreferencesModel(
      //TODO: revisar como lo entrega
        List<bool>.from(json['alergias']),
        json['ubicacionHogar'],
        json['celiaco'] as bool,
        List<String>.from(json['deportes']),
        json['vegano'] as bool,
        json['vegetariano'] as bool ) ;
  }

  Map<String, dynamic> toJson()
  {
    return {
      "alergias": alergias,
      "ubicacionHogar": ubicacionHogar,
      "celiaco": celiaco,
      "deportes": deportes,
      "vegano": vegano,
      "vegetariano": vegetariano,

    };
  }
}
