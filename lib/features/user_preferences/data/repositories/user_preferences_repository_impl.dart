import 'package:corazon_saludable/core/error/exeptions.dart';
import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/network/network_info.dart';
import 'package:corazon_saludable/features/user_preferences/data/data_sources/user_preferences_local_data_source.dart';
import 'package:corazon_saludable/features/user_preferences/data/data_sources/user_preferences_remote_data_source.dart';
import 'package:corazon_saludable/features/user_preferences/data/models/user_preferences_model.dart';
import 'package:corazon_saludable/features/user_preferences/domain/entities/user_preferences.dart';
import 'package:corazon_saludable/features/user_preferences/domain/repositories/user_preferences_repository.dart';
import 'package:dartz/dartz.dart';

class UserPreferencesRepositoryImpl implements UserPreferencesRepository {
  //reference of the local data source
  final UserPreferencesLocalDataSource localDataSource;

//reference of the remote data source
  final UserPreferencesRemoteDataSource remoteDataSource;

//reference to network info, service that checks is the device is onLine
  final NetworkInfo networkInfo;

  UserPreferencesRepositoryImpl(
      this.localDataSource, this.remoteDataSource, this.networkInfo);

  @override
  Future<Either<Failure, UserPreferences>> getUserPreferences() async {
    if (await networkInfo.isConnected) {
      try {
        UserPreferencesModel remoteUserData = await remoteDataSource.getUserPreferences();

        localDataSource.cacheUserPreference(remoteUserData);
        return Right(remoteUserData);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localPreferences = await localDataSource.getLastUserPreference();
        return Right(localPreferences);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, UserPreferences>> setUserPreferences(userPreferences) async {
    if (await networkInfo.isConnected) {
      try {
        final modelo = UserPreferencesModel(
            userPreferences.alergias,
            userPreferences.ubicacionHogar,
            userPreferences.celiaco,
            userPreferences.deportes,
            userPreferences.vegano,
            userPreferences.vegetariano);


        final remoteUserData = await remoteDataSource.addUserPreference(modelo);
        localDataSource.cacheUserPreference(modelo);
        //! print to see the result of the local storage
        return Right(remoteUserData);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }
}