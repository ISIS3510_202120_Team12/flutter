import 'package:equatable/equatable.dart';

/**
 * this class represents the user preferences
 *
 */
class UserPreferences extends Equatable{

  /**
   * Alergies list
   * 0 -> huevos
   * 1 -> pescado
   * 2 -> leche
   * 3 -> manis
   * 4 -> mariscos
   * 5 -> soya
   * 6 -> nueces
   * 7 -> trigo
   * 8 -> fresa
   */

  //final String id;
  final List <bool> alergias;
  final String ubicacionHogar;
  final bool celiaco;
  final List  <String> deportes;
  final bool vegano;
  final bool vegetariano;

  UserPreferences(this.alergias, this.ubicacionHogar, this.celiaco, this.deportes, this.vegano, this.vegetariano);
  @override
  // TODO: implement props
  List<Object?> get props => [alergias, ubicacionHogar, celiaco, deportes, vegano, vegetariano];
}
