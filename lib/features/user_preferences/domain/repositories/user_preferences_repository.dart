import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/features/user_preferences/domain/entities/user_preferences.dart';
import 'package:dartz/dartz.dart';

/**
 * this class is a contract that must be implemented in the data layer
 * it handles what methods must be implemented in order to let the
 * app work
 */
abstract class UserPreferencesRepository{
  Future<Either<Failure, UserPreferences>> getUserPreferences();
  Future<Either<Failure, UserPreferences>> setUserPreferences(UserPreferences);
//Future<Either<Failure, UserPreferences>> postUserPreferences(UserPreferences);
}