import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/usecases/usecase.dart';
import 'package:corazon_saludable/features/user_preferences/domain/entities/user_preferences.dart';
import 'package:corazon_saludable/features/user_preferences/domain/repositories/user_preferences_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class GetUserPreferences implements UseCase<UserPreferences, NoParams>
{
  final UserPreferencesRepository repository;

  GetUserPreferences(this.repository);

  @override
  Future<Either<Failure, UserPreferences>> call(NoParams params) async {
    return await repository.getUserPreferences();
  }

}
