import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/usecases/usecase.dart';
import 'package:corazon_saludable/features/user_preferences/domain/entities/user_preferences.dart';
import 'package:corazon_saludable/features/user_preferences/domain/repositories/user_preferences_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class SetUserPreferences implements UseCase<UserPreferences, Params>
{
  final UserPreferencesRepository repository;

  SetUserPreferences(this.repository);

  @override
  Future<Either<Failure, UserPreferences>> call(Params params) {
    return repository.setUserPreferences(params.toSave);
  }

}

class Params extends Equatable
{
  final UserPreferences toSave;
  //TODO: revisar la integridad de esta parte del código
  const Params(this.toSave);
  @override
  List<Object> get props => [toSave];
}
