import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/usecases/usecase.dart';
import 'package:corazon_saludable/features/user_preferences/domain/entities/user_preferences.dart';
import 'package:corazon_saludable/features/user_preferences/domain/use_cases/get_user_preferences.dart';
import 'package:corazon_saludable/features/user_preferences/domain/use_cases/set_user_preferences.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
part 'user_preferences_event.dart';
part 'user_preferences_state.dart';

const String SERVER_FAILURE_MESSAGE = 'Actualmente no cuentas con conexión a internet.';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';
class UserPreferencesBloc extends Bloc<UserPreferencesEvent, UserPreferencesState> {
  final GetUserPreferences getUserPreferences;
  final SetUserPreferences setUserPreferences;
  UserPreferencesBloc(
      {required GetUserPreferences getPrefs,
        required SetUserPreferences setPrefs,})
      : assert(getPrefs != null),
        assert(setPrefs != null),
        getUserPreferences = getPrefs,
        setUserPreferences = setPrefs, super(Empty());

  /// this method maps each event thrown from the UI to an specific state
  @override
  Stream<UserPreferencesState> mapEventToState(
      UserPreferencesEvent event,
      ) async* {
    if(event is GetUserPreferencesEvent){
      yield Loading();
      final failureOrPrefs = await getUserPreferences(NoParams());
      yield* _eitherLoadedOrErrorStateGet(failureOrPrefs);
    } if(event is SetUserPreferencesEvent)
    {
      yield Loading();
      final failureOrUserPrefs = await setUserPreferences(Params(event.userPreferences));
      yield* _eitherLoadedOrErrorStateSend(failureOrUserPrefs);
    }
  }

  /// Rodrigo: this method handles de functional programming that we used
  /// with the library 'dartz'. By the implemented standard, the left side
  /// @returns a failure when there is any kind of error, while the right one
  /// @returns an entity of UserBasicInfo type that contains the user data
  /// the final @return of this method is a new state for this Bloc.
  Stream<UserPreferencesState> _eitherLoadedOrErrorStateGet(
      Either<Failure, UserPreferences> gottenPrfs
      ) async*{
    yield gottenPrfs.fold(
          (failure) => Error(_mapFailureToMessage(failure)),
          (userPreferences) => Loaded(userPreferences),);
  }
  /// Rodrigo: this method behaves similar to _eitherLoadedOrErrorStateGet but it
  /// uses a different state that represents that the data was sent to the
  /// firabase storage
  Stream<UserPreferencesState> _eitherLoadedOrErrorStateSend(
      Either<Failure, UserPreferences> gottenPrefs
      ) async*{
    yield gottenPrefs.fold(
          (failure) => Error(_mapFailureToMessage(failure)),
          (userPreferences) => Sent(userPreferences),);
  }
  /// Rodrigo: this method maps the different kinds of error to it's specific
  /// messages

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }

}

