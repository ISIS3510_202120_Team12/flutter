part of 'user_preferences_bloc.dart';

@immutable
abstract class UserPreferencesEvent extends Equatable{
  @override
  List<Object> get props => [];
}
class GetUserPreferencesEvent extends UserPreferencesEvent{}

class SetUserPreferencesEvent extends UserPreferencesEvent{
  final UserPreferences userPreferences;

  SetUserPreferencesEvent(this.userPreferences);

  @override
  List<Object> get props => [userPreferences];

}