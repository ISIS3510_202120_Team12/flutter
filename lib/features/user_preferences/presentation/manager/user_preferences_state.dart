part of 'user_preferences_bloc.dart';

@immutable
abstract class UserPreferencesState extends Equatable {
  @override
  List<Object> get props => [];
}

class UserPreferencesInitial extends UserPreferencesState {}

// no state at all
class Empty extends UserPreferencesState {}

// the app is trying to load an object
class Loading extends UserPreferencesState {}

// the application performed the action
class Loaded extends UserPreferencesState {
  final UserPreferences userPreferences;

  Loaded(this.userPreferences);

  @override
  List<Object> get props => [userPreferences];
}

// the aplication had an error
class Error extends UserPreferencesState {
  final String message;

  Error(this.message);

  @override
  List<Object> get props => [message];
}
class Sent extends UserPreferencesState{
  final UserPreferences userPreferences;

  Sent(this.userPreferences);

}
