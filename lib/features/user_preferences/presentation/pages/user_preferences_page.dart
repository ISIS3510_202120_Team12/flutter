

import 'package:corazon_saludable/UI/main_menu.dart';
import 'package:corazon_saludable/features/user_preferences/presentation/widgets/display_message_widget.dart';
import 'package:corazon_saludable/features/user_preferences/presentation/widgets/loading_widget.dart';
import 'package:corazon_saludable/features/user_preferences/presentation/manager/user_preferences_bloc.dart';
import 'package:corazon_saludable/features/user_preferences/presentation/widgets/user_preferences_form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../injection_container.dart';

class UserPreferencesPage extends StatelessWidget {
/*
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Number Trivia'),
      ),
      body: SingleChildScrollView(

      ),
    );
  }
*/
  bool iniciarLleno;
  UserPreferencesPage( {Key? key, bool iniciar = false }): iniciarLleno = iniciar,  super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) =>  MainMenu())),
          ),
          title: const Text("Tus preferencias"),
          backgroundColor: const Color(0xff002d72),
        ),
        body: SingleChildScrollView(
          child: buildBody(context),
        ));
  }

  BlocProvider<UserPreferencesBloc> buildBody(BuildContext context) {
    return BlocProvider(
      create: (_) => sl<UserPreferencesBloc>(),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(3),
          child: Column(
            children: <Widget>[
              const SizedBox(height: 1),
              BlocBuilder<UserPreferencesBloc,UserPreferencesState>(
                  builder: (context, state){
                    if(state is Empty){
                      if(iniciarLleno){
                        BlocProvider.of<UserPreferencesBloc>(context).add(GetUserPreferencesEvent());
                        print("inició en true");
                      }
                      return UserPreferencesDataForm();
                    }
                    else if(state is Loading){
                      print("LOADING");
                      return const LoadingWidget();
                    }
                    else if(state is Loaded){
                      print("LOADED");
                      return UserPreferencesDataForm(valorPorDefecto: state.userPreferences);
                    }
                    else if(state is Error){
                      return MessageDisplay(message: state.message);
                    }
                    else if(state is Sent) {
                      return MessageDisplay(message: "Información actualizada con éxito");
                    }
                    else{
                      return MessageDisplay(message: "Estado indefinido");
                    }
                  })
            ],
          ),
        ),
      ),
    );
  }

}