

import 'package:corazon_saludable/core/firebase/firebase_analytics.dart';
import 'package:corazon_saludable/features/user_preferences/domain/entities/user_preferences.dart';
import 'package:corazon_saludable/features/user_preferences/presentation/manager/user_preferences_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

import '../../../../injection_container.dart';

class UserPreferencesDataForm extends StatelessWidget {
  final _formKey = GlobalKey<FormBuilderState>();
  UserPreferences userPreferences;


  UserPreferencesDataForm({Key? key, UserPreferences? valorPorDefecto}) :
        userPreferences = valorPorDefecto  ?? UserPreferences([], "", true, [], true, true) ,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print(userPreferences);
    //initialValue: _getInitialValueDictionaty(userPreferences);

    return SizedBox(
      height: MediaQuery.of(context).size.height / 1.1,
      child: Scaffold(
        body: FormBuilder(
          key: _formKey,
          child: SingleChildScrollView(
            child : Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  const SizedBox(height: 35),
                  FormBuilderTextField(
                    name: "ubicacionHogar",
                    decoration: const InputDecoration(
                        labelText: "Ingresa tu Direccion ",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        ),
                        filled: true,
                        hintStyle: TextStyle(color: Color(0xFF424242)),
                        hintText: "Inserte su direccion completa",
                        fillColor: Colors.white70),
                    validator: FormBuilderValidators.required(context),
                  ),
                  FormBuilderTextField(name: "RestriccionesAlimDiv",
                    readOnly: true ,
                    initialValue: 'Seleccione sus restricciones alimenticias',),
                  FormBuilderCheckbox(
                      name: "celiaco",
                      title: const Text("¿Es usted Celiaco?")),
                  FormBuilderCheckbox(
                      name: "vegano",
                      title: const Text("¿Es usted Vegano?")),
                  FormBuilderCheckbox(
                      name: "vegetariano",
                      title: const Text("¿Es usted Vegetariano?")),
                  FormBuilderTextField(name: "AlergiasDiv",
                    readOnly: true ,
                    initialValue: 'Seleccione sus alergias',),
                  FormBuilderSwitch(
                      name: 'Switch0',
                      initialValue: userPreferences.alergias[0] ,
                      title: Text('Huevos')),
                  FormBuilderSwitch(
                      name: 'Switch1',
                      initialValue: userPreferences.alergias[1] ,
                      title: Text('pescado')),
                  FormBuilderSwitch(
                      name: 'Switch0',
                      initialValue: userPreferences.alergias[2] ,
                      title: Text('Leche')),
                  FormBuilderSwitch(
                      name: 'Switch1',
                      initialValue: userPreferences.alergias[3] ,
                      title: Text('Mani')),
                  FormBuilderSwitch(
                      name: 'Switch0',
                      initialValue: userPreferences.alergias[4] ,
                      title: Text('Mariscos')),
                  FormBuilderSwitch(
                      name: 'Switch1',
                      initialValue: userPreferences.alergias[5] ,
                      title: Text('Soya')),
                  FormBuilderSwitch(
                      name: 'Switch0',
                      initialValue: userPreferences.alergias[6] ,
                      title: Text('Nueces')),
                  FormBuilderSwitch(
                      name: 'Switch1',
                      initialValue: userPreferences.alergias[7] ,
                      title: Text('Trigo')),
                  FormBuilderSwitch(
                      name: 'Switch0',
                      initialValue: userPreferences.alergias[8] ,
                      title: Text('Fresas')),








                  //!revisar el botón con gradiente
                  const SizedBox(height: 30),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(4),
                    child: Stack(
                      children: <Widget>[
                        Positioned.fill(
                          child: Container(
                            decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                colors: <Color>[
                                  Color(0xFF0D47A1),
                                  Color(0xFF1976D2),
                                  Color(0xff002d72),
                                ],
                              ),
                            ),
                          ),
                        ),
                        TextButton(
                          style: TextButton.styleFrom(
                            padding: const EdgeInsets.all(16.0),
                            primary: Colors.white,
                            textStyle: const TextStyle(fontSize: 20),
                          ),
                          onPressed: () {
                            final validationSucess = _formKey.currentState!.validate();
                            if(validationSucess)
                            {
                              //! the current form data is saved (only in the form context)
                              _formKey.currentState?.save();
                              //! the data is extracted
                              final formData = _formKey.currentState?.value;
                              sl<AnalyticService>().dataUpdated(nombre: formData!["ubicacionHogar"]);
                              var cosita = UserPreferences(
                                List<bool>.from(formData["alergias"]),
                                formData["ubicacionHogar"],
                                formData["celiaco"] as bool,
                                List<String>.from(formData["deportes"]),
                                formData["vegano"] as bool,
                                formData["vegetariano"] as bool,);
                              BlocProvider.of<UserPreferencesBloc>(context).add(SetUserPreferencesEvent(cosita));
                              //! finally the data is shown
                              FocusScope.of(context).unfocus();
                            }
                          },
                          child: const Text('Guardar'),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 40,)],
              ),
            ),),
          autovalidateMode: AutovalidateMode.onUserInteraction,
          //! this initial value works as a dictionary that uses each field key
          //! to give them an initial value {"nombre": usuarioActual.nombreCompleto }
          initialValue: _getInitialValueDictionaty(userPreferences) ,
        ),
      ),
    );
  }

  _getInitialValueDictionaty(UserPreferences prf){
    Map<String, Object> datos= {"ubicacionHogar":"",
    };

    print("Adentro");
    print(prf);
    print(userPreferences);
    if(prf.ubicacionHogar == "" ){
      print("DATOS");
      print(datos);
      return datos;
    }
    else{ datos ={"alergias":prf.alergias,
      "ubicacionHogar" : prf.ubicacionHogar,
      "celiaco":prf.celiaco,
      "deportes": prf.deportes,
      "vegano": prf.vegano,
      "vegetariano": prf.vegetariano};
    return datos;
    }
  }

}