import 'dart:convert';

import 'package:corazon_saludable/core/error/exeptions.dart';
import 'package:corazon_saludable/features/user_tips/data/models/user_tips_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class UserTipsLocalDataSource {
  /// Gets the cached [UserTipsModel] which was gotten the last time
  /// the user had an internet connection.
  ///
  /// Throws [CacheException] if no cached data is present.
  Future<UserTipsModel> getLastUserTips();

  Future<void> cacheUserTips(UserTipsModel userTipsToCache);
}
const CACHED_USER_TIPS = 'CACHED_USER_TIPS';

class UserTipsLocalDataSourceImpl implements UserTipsLocalDataSource{
  final SharedPreferences sharedPreferences;

  UserTipsLocalDataSourceImpl({required SharedPreferences preferences})
      : sharedPreferences = preferences;

  @override
  Future<void> cacheUserTips (UserTipsModel userTipsToCache) {
    return sharedPreferences.setString(
        CACHED_USER_TIPS,
        json.encode(userTipsToCache.toJson()
        )
    );
  }

  @override
  Future<UserTipsModel> getLastUserTips() {
    final jsonString = sharedPreferences.getString(CACHED_USER_TIPS);
    if(jsonString != null){
      return Future.value(UserTipsModel.fromJson(json.decode(jsonString)));
    } else {
      throw CacheException();
    }
  }

}