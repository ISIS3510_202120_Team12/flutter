import 'dart:convert';
import 'package:corazon_saludable/core/error/exeptions.dart';
import 'package:corazon_saludable/features/user_tips/data/models/user_tips_model.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:http/http.dart' as http;

abstract class UserTipsRemoteDataSource{
  /// Calls the https://us-central1-backendcorazonsaludable.cloudfunctions.net/tips/{number} endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<UserTipsModel> getUserTips();

  /// Posts new user info https://us-central1-backendcorazonsaludable.cloudfunctions.net/tips/{number}endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<UserTipsModel> addUserTips(UserTipsModel modelo);

}


class UserTipsRemoteDataSourceImpl implements UserTipsRemoteDataSource{
  final http.Client client;
  String? logueado =  FirebaseAuth.instance.currentUser?.uid;
  late String apiUrl = "https://us-central1-backendcorazonsaludable.cloudfunctions.net/tips/5zH2yLNb7F5qfTFgrNrk";
  late Uri uriApi = Uri.parse(apiUrl);
  UserTipsRemoteDataSourceImpl(this.client);

  @override
  Future<UserTipsModel> addUserTips(UserTipsModel modelo) async
  {
    final response  = await client.put(uriApi,headers: <String, String>
    {
      'Content-Type': 'application/json; charset=UTF-8',
    }, body: json.encode(modelo.toJson()) );
    if(response.statusCode == 201){
      return modelo;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<UserTipsModel> getUserTips() async {
    final response = await client.get(uriApi,
      headers: {
        'Content-Type': 'application/json',
      },);
    if (response.statusCode ==200){
      return UserTipsModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}