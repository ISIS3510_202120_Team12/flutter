import 'package:corazon_saludable/features/user_tips/domain/entities/user_tips.dart';

class UserTipsModel extends UserTips
{
  UserTipsModel(
      List <bool> constraints,
      String descripcion,
      String nombre,
      String tipo
      )
      : super(constraints,descripcion , nombre,tipo );

  factory UserTipsModel.fromJson(Map<String, dynamic> json)
  {
    return UserTipsModel(
      //TODO: revisar como lo entrega
        json['constraints'],
        json['descripción'],
        json['nombre'],
        json['tipo']) ;
  }

  Map<String, dynamic> toJson()
  {
    return {
      "constraints": constraints,
      "descripción": descripcion,
      "nombre": nombre,
      "tipo": tipo,

    };
  }
}
