import 'package:corazon_saludable/core/error/exeptions.dart';
import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/network/network_info.dart';
import 'package:corazon_saludable/features/user_tips/data/data_sources/user_tips_local_data_source.dart';
import 'package:corazon_saludable/features/user_tips/data/data_sources/user_tips_remote_data_source.dart';
import 'package:corazon_saludable/features/user_tips/data/models/user_tips_model.dart';
import 'package:corazon_saludable/features/user_tips/domain/entities/user_tips.dart';
import 'package:corazon_saludable/features/user_tips/domain/repositories/user_tips_repository.dart';
import 'package:dartz/dartz.dart';

class UserTipsRepositoryImpl implements UserTipsRepository {
  //reference of the local data source
  final UserTipsLocalDataSource localDataSource;

//reference of the remote data source
  final UserTipsRemoteDataSource remoteDataSource;

//reference to network info, service that checks is the device is onLine
  final NetworkInfo networkInfo;

  UserTipsRepositoryImpl(
      this.localDataSource, this.remoteDataSource, this.networkInfo);

  @override
  Future<Either<Failure, UserTips>> getUserTips() async {
    if (await networkInfo.isConnected) {
      try {
        UserTipsModel remoteUserData = await remoteDataSource.getUserTips();
        //Print lista
        print (Right(remoteUserData));
        localDataSource.cacheUserTips(remoteUserData);
        return Right(remoteUserData);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localTips = await localDataSource.getLastUserTips();
        print (Right(localTips));
        return Right(localTips);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, UserTips>> setUserTips(userTips) async {
    if (await networkInfo.isConnected) {
      try {
        final modelo = UserTipsModel(
            userTips.userTips,
            userTips.descripcion,
            userTips.nombre,
            userTips.tipo);


        final remoteUserData = await remoteDataSource.addUserTips(modelo);
        localDataSource.cacheUserTips(modelo);
        //! print to see the result of the local storage
        return Right(remoteUserData);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }
}