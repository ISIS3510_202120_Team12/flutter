import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

/**
 * this class represents the user preferences
 *
 */
class UserTips extends Equatable{

  /**
   * Alergies list
   * 0 -> huevos
   * 1 -> pescado
   * 2 -> leche
   * 3 -> manis
   * 4 -> mariscos
   * 5 -> soya
   * 6 -> nueces
   * 7 -> trigo
   * 8 -> fresa
   * 9 -> celiaco
   * 10 -> vegano
   * 11 -> vegetariano
   * 12 -> diabetico
   */

  final List <bool> constraints;
  final String descripcion;
  final String nombre;
  final String tipo;

  UserTips(this.constraints, this.descripcion, this.nombre, this.tipo);
  @override
  // TODO: implement props
  List<Object?> get props => [nombre, tipo, descripcion];
}
