import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/features/user_tips/domain/entities/user_tips.dart';
import 'package:dartz/dartz.dart';

/**
 * this class is a contract that must be implemented in the data layer
 * it handles what methods must be implemented in order to let the
 * app work
 */
abstract class UserTipsRepository{
  Future<Either<Failure, UserTips>> getUserTips();
  Future<Either<Failure, UserTips>> setUserTips(UserTips);
//Future<Either<Failure, UserPreferences>> postUserPreferences(UserPreferences);
}
