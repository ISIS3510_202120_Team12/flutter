import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/usecases/usecase.dart';
import 'package:corazon_saludable/features/user_tips/domain/entities/user_tips.dart';
import 'package:corazon_saludable/features/user_tips/domain/repositories/user_tips_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class GetUserTips implements UseCase<UserTips, NoParams>
{
  final UserTipsRepository repository;

  GetUserTips(this.repository);

  @override
  Future<Either<Failure, UserTips>> call(NoParams params) async {
    return await repository.getUserTips();
  }

}