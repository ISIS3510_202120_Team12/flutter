import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/usecases/usecase.dart';
import 'package:corazon_saludable/features/user_tips/domain/entities/user_tips.dart';
import 'package:corazon_saludable/features/user_tips/domain/use_cases/get_user_tips.dart';
import 'package:corazon_saludable/features/user_tips/domain/use_cases/set_user_tips.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
part 'user_tips_event.dart';
part 'user_tips_state.dart';

const String SERVER_FAILURE_MESSAGE = 'Actualmente no cuentas con conexión a internet.';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';
class UserTipsBloc extends Bloc<UserTipsEvent, UserTipsState> {
  final GetUserTips getUserTips;
  final SetUserTips setUserTips;
  UserTipsBloc(
      {required GetUserTips getPrefs,
        required SetUserTips setPrefs,})
      : assert(getPrefs != null),
        assert(setPrefs != null),
        getUserTips = getPrefs,
        setUserTips = setPrefs, super(Empty());

  /// this method maps each event thrown from the UI to an specific state
  @override
  Stream<UserTipsState> mapEventToState(
      UserTipsEvent event,
      ) async* {
    if(event is GetUserTipsEvent){
      yield Loading();
      final failureOrPrefs = await getUserTips(NoParams());
      yield* _eitherLoadedOrErrorStateGet(failureOrPrefs);
    } if(event is SetUserTipsEvent)
    {
      yield Loading();
      final failureOrUserPrefs = await setUserTips(Params(event.userTips));
      yield* _eitherLoadedOrErrorStateSend(failureOrUserPrefs);
    }
  }

  /// Rodrigo: this method handles de functional programming that we used
  /// with the library 'dartz'. By the implemented standard, the left side
  /// @returns a failure when there is any kind of error, while the right one
  /// @returns an entity of UserBasicInfo type that contains the user data
  /// the final @return of this method is a new state for this Bloc.
  Stream<UserTipsState> _eitherLoadedOrErrorStateGet(
      Either<Failure, UserTips> gottenPrfs
      ) async*{
    yield gottenPrfs.fold(
          (failure) => Error(_mapFailureToMessage(failure)),
          (userTips) => Loaded(userTips),);
  }
  /// Rodrigo: this method behaves similar to _eitherLoadedOrErrorStateGet but it
  /// uses a different state that represents that the data was sent to the
  /// firabase storage
  Stream<UserTipsState> _eitherLoadedOrErrorStateSend(
      Either<Failure, UserTips> gottenPrefs
      ) async*{
    yield gottenPrefs.fold(
          (failure) => Error(_mapFailureToMessage(failure)),
          (userTips) => Sent(userTips),);
  }
  /// Rodrigo: this method maps the different kinds of error to it's specific
  /// messages

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }

}
