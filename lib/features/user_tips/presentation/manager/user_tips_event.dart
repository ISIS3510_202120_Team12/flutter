part of 'user_tips_bloc.dart';

@immutable
abstract class UserTipsEvent extends Equatable{
  @override
  List<Object> get props => [];
}
class GetUserTipsEvent extends UserTipsEvent{}

class SetUserTipsEvent extends UserTipsEvent{
  final UserTips userTips;

  SetUserTipsEvent(this.userTips);

  @override
  List<Object> get props => [userTips];

}
