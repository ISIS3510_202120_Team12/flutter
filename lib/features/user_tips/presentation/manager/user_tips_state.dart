part of 'user_tips_bloc.dart';

@immutable
abstract class UserTipsState extends Equatable {
  @override
  List<Object> get props => [];
}

class UserTipsInitial extends UserTipsState {}

// no state at all
class Empty extends UserTipsState {}

// the app is trying to load an object
class Loading extends UserTipsState {}

// the application performed the action
class Loaded extends UserTipsState {
  final UserTips userTips;
  Loaded(this.userTips);

  @override
  List<Object> get props => [userTips];
}
// the aplication had an error
class Error extends UserTipsState {
  final String message;

  Error(this.message);

  @override
  List<Object> get props => [message];
}
class Sent extends UserTipsState{
  final UserTips userTips;
  Sent(this.userTips);
}
