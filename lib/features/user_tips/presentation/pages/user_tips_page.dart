

import 'package:corazon_saludable/UI/main_menu.dart';
import 'package:corazon_saludable/features/user_tips/presentation/widgets/display_message_widget.dart';
import 'package:corazon_saludable/features/user_tips/presentation/widgets/loading_widget.dart';
import 'package:corazon_saludable/features/user_tips/presentation/manager/user_tips_bloc.dart';
import 'package:corazon_saludable/features/user_tips/presentation/widgets/user_tips_form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../injection_container.dart';

class UserTipsPage extends StatelessWidget {
/*
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Number Trivia'),
      ),
      body: SingleChildScrollView(

      ),
    );
  }
*/
  bool iniciarLleno;
  UserTipsPage( {Key? key, bool iniciar = false }): iniciarLleno = iniciar,  super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) =>  MainMenu())),
          ),
          title: const Text("Tus Tip Personalizados"),
          backgroundColor: const Color(0xff002d72),
        ),
        body: SingleChildScrollView(
          child: buildBody(context),
        ));
  }

  BlocProvider<UserTipsBloc> buildBody(BuildContext context) {
    return BlocProvider(
      create: (_) => sl<UserTipsBloc>(),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(3),
          child: Column(
            children: <Widget>[
              const SizedBox(height: 1),
              BlocBuilder<UserTipsBloc,UserTipsState>(
                  builder: (context, state){
                    if(state is Empty){
                      if(iniciarLleno){
                        BlocProvider.of<UserTipsBloc>(context).add(GetUserTipsEvent());
                        print("inició en true");
                      }
                      return UserTipsDataForm();
                    }
                    else if(state is Loading){
                      print("LOADING");
                      return const LoadingWidget();
                    }
                    else if(state is Loaded){
                      print("LOADED");
                      return UserTipsDataForm(valorPorDefecto: state.userTips);
                    }
                    else if(state is Error){
                      return MessageDisplay(message: state.message);
                    }
                    else if(state is Sent) {
                      return MessageDisplay(message: "Información actualizada con éxito");
                    }
                    else{
                      return MessageDisplay(message: "Estado indefinido");
                    }
                  })
            ],
          ),
        ),
      ),
    );
  }

}