

import 'package:corazon_saludable/core/firebase/firebase_analytics.dart';
import 'package:corazon_saludable/features/user_tips/domain/entities/user_tips.dart';
import 'package:corazon_saludable/features/user_tips/presentation/manager/user_tips_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

import '../../../../injection_container.dart';

class UserTipsDataForm extends StatelessWidget {
  final _formKey = GlobalKey<FormBuilderState>();
  UserTips userTips;


  UserTipsDataForm({Key? key, UserTips? valorPorDefecto}) :
        userTips = valorPorDefecto  ?? UserTips([], "", "nombre", "tipo"),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    // TODO: implement build
    print(userTips);
    initialValue: _getInitialValueDictionaty(userTips);
    print(_getInitialValueDictionaty(userTips));

    return Column(
      children: [
        Container (
          child: SizedBox(
            height: 210,
            child: InkWell(
              child: Stack(
                children: [
                  Positioned(
                    top: 35,
                    left: 20,
                    child: Material(
                      child: Container(
                        height: 160.0,
                        width: width * 0.9,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(0.0),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.3),
                                offset: const Offset(-10.0, 10.0),
                                blurRadius: 20.0,
                                spreadRadius: 4.0)
                          ],
                        ),
                      ),

                    ),),
                  Positioned(
                      top: 0,
                      left: 30,
                      child: Card(
                        color: const Color(0xffffffff),
                        elevation: 10.0,
                        shadowColor: Colors.grey.withOpacity(0.5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Container(
                          height: 180,
                          width: 150,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              image: const DecorationImage(
                                  fit: BoxFit.fill,
                                  image: AssetImage(
                                      "lib/core/images/tips.png"))),
                        ),

                      )),
                  Positioned(
                      top: 58,
                      left: 200,
                      child: Container(
                        height: 160,
                        width: 180, //! puede ser cambiada por 160 para ver que la barra no moleste
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              "Actividad Física",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Color(0xffe30046),
                                  fontWeight: FontWeight.bold),
                            ),
                            const Text(
                              "Trotar 20 minutos",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                            Divider(color: Colors.grey.shade500,),
                            const Text(
                              "Intermedia",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),

                      )),
                ],
              ),
            ),
          ),
        ),Container (
            child: SizedBox(
              height: 210,
              child: InkWell(
                child: Stack(
                  children: [
                    Positioned(
                      top: 35,
                      left: 20,
                      child: Material(
                        child: Container(
                          height: 160.0,
                          width: width * 0.9,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(0.0),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.3),
                                  offset: const Offset(-10.0, 10.0),
                                  blurRadius: 20.0,
                                  spreadRadius: 4.0)
                            ],
                          ),
                        ),

                      ),),
                    Positioned(
                        top: 0,
                        left: 30,
                        child: Card(
                          color: const Color(0xffffffff),
                          elevation: 10.0,
                          shadowColor: Colors.grey.withOpacity(0.5),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0)),
                          child: Container(
                            height: 180,
                            width: 150,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                image: const DecorationImage(
                                    fit: BoxFit.fill,
                                    image: AssetImage(
                                        "lib/core/images/tips.png"))),
                          ),

                        )),
                    Positioned(
                        top: 58,
                        left: 200,
                        child: Container(
                          height: 160,
                          width: 180, //! puede ser cambiada por 160 para ver que la barra no moleste
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Actividad Física",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Color(0xffe30046),
                                    fontWeight: FontWeight.bold),
                              ),
                              const Text(
                                "Camina 2000 pasos",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold),
                              ),
                              Divider(color: Colors.grey.shade500,),
                              const Text(
                                "Complicada",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),

                        )),
                  ],
                ),
              ),
            ),
        ),Container (
          child: SizedBox(
            height: 210,
            child: InkWell(
              child: Stack(
                children: [
                  Positioned(
                    top: 35,
                    left: 20,
                    child: Material(
                      child: Container(
                        height: 160.0,
                        width: width * 0.9,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(0.0),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.3),
                                offset: const Offset(-10.0, 10.0),
                                blurRadius: 20.0,
                                spreadRadius: 4.0)
                          ],
                        ),
                      ),

                    ),),
                  Positioned(
                      top: 0,
                      left: 30,
                      child: Card(
                        color: const Color(0xffffffff),
                        elevation: 10.0,
                        shadowColor: Colors.grey.withOpacity(0.5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Container(
                          height: 180,
                          width: 150,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              image: const DecorationImage(
                                  fit: BoxFit.fill,
                                  image: AssetImage(
                                      "lib/core/images/tips.png"))),
                        ),

                      )),
                  Positioned(
                      top: 58,
                      left: 200,
                      child: Container(
                        height: 160,
                        width: 180, //! puede ser cambiada por 160 para ver que la barra no moleste
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              "Actividad Física",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Color(0xffe30046),
                                  fontWeight: FontWeight.bold),
                            ),
                            const Text(
                              "Trotar 60 minutos",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                            Divider(color: Colors.grey.shade500,),
                            const Text(
                              "Complicada",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),

                      )),
                ],
              ),
            ),
          ),
        ),Container (
          child: SizedBox(
            height: 210,
            child: InkWell(
              child: Stack(
                children: [
                  Positioned(
                    top: 35,
                    left: 20,
                    child: Material(
                      child: Container(
                        height: 160.0,
                        width: width * 0.9,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(0.0),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.3),
                                offset: const Offset(-10.0, 10.0),
                                blurRadius: 20.0,
                                spreadRadius: 4.0)
                          ],
                        ),
                      ),

                    ),),
                  Positioned(
                      top: 0,
                      left: 30,
                      child: Card(
                        color: const Color(0xffffffff),
                        elevation: 10.0,
                        shadowColor: Colors.grey.withOpacity(0.5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Container(
                          height: 180,
                          width: 150,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              image: const DecorationImage(
                                  fit: BoxFit.fill,
                                  image: AssetImage(
                                      "lib/core/images/tips.png"))),
                        ),

                      )),
                  Positioned(
                      top: 58,
                      left: 200,
                      child: Container(
                        height: 160,
                        width: 180, //! puede ser cambiada por 160 para ver que la barra no moleste
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              "Actividad Física",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Color(0xffe30046),
                                  fontWeight: FontWeight.bold),
                            ),
                            const Text(
                              "Camina 2000 pasos",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                            Divider(color: Colors.grey.shade500,),
                            const Text(
                              "Sencilla",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),

                      )),
                ],
              ),
            ),
          ),
        ),Container (
          child: SizedBox(
            height: 210,
            child: InkWell(
              child: Stack(
                children: [
                  Positioned(
                    top: 35,
                    left: 20,
                    child: Material(
                      child: Container(
                        height: 160.0,
                        width: width * 0.9,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(0.0),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.3),
                                offset: const Offset(-10.0, 10.0),
                                blurRadius: 20.0,
                                spreadRadius: 4.0)
                          ],
                        ),
                      ),

                    ),),
                  Positioned(
                      top: 0,
                      left: 30,
                      child: Card(
                        color: const Color(0xffffffff),
                        elevation: 10.0,
                        shadowColor: Colors.grey.withOpacity(0.5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Container(
                          height: 180,
                          width: 150,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              image: const DecorationImage(
                                  fit: BoxFit.fill,
                                  image: AssetImage(
                                      "lib/core/images/tips.png"))),
                        ),

                      )),
                  Positioned(
                      top: 58,
                      left: 200,
                      child: Container(
                        height: 160,
                        width: 180, //! puede ser cambiada por 160 para ver que la barra no moleste
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              "Actividad Física",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Color(0xffe30046),
                                  fontWeight: FontWeight.bold),
                            ),
                            const Text(
                              "Camina 3000 pasos",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                            Divider(color: Colors.grey.shade500,),
                            const Text(
                              "Sencilla",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),

                      )),
                ],
              ),
            ),
          ),
        ),


      ],
    );

  }


  _getInitialValueDictionaty(UserTips prf){
    Map<String, Object> datos= {"descripción":"",
    };

    print("Adentro");
    print(prf);
    print(userTips);
    if(prf.descripcion == "" ){
      print("DATOS");
      print(datos);
      return datos;
    }
    else{
    return datos;
    }
  }



}