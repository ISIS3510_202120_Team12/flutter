import 'package:corazon_saludable/core/firebase/firebase_analytics.dart';
import 'package:corazon_saludable/core/network/network_info.dart';
import 'package:corazon_saludable/features/user_basic_data/data/data_sources/user_basic_info_local_data_source.dart';
import 'package:corazon_saludable/features/user_basic_data/data/data_sources/user_basic_info_remote_data_source.dart';
import 'package:corazon_saludable/features/user_basic_data/data/repositories/user_basic_info_repository_impl.dart';
import 'package:corazon_saludable/features/user_basic_data/domain/repositories/user_basic_info_repository.dart';
import 'package:corazon_saludable/features/user_basic_data/domain/use_cases/get_user_basic_info.dart';
import 'package:corazon_saludable/features/user_basic_data/domain/use_cases/set_user_data_basic_info.dart';
import 'package:corazon_saludable/features/user_basic_data/presentation/manager/user_basic_info_bloc.dart';
import 'package:corazon_saludable/features/user_preferences/data/data_sources/user_preferences_remote_data_source.dart';
import 'package:corazon_saludable/features/user_preferences/data/repositories/user_preferences_repository_impl.dart';
import 'package:corazon_saludable/features/user_preferences/domain/repositories/user_preferences_repository.dart';
import 'package:corazon_saludable/features/user_preferences/domain/use_cases/get_user_preferences.dart';
import 'package:corazon_saludable/features/user_preferences/domain/use_cases/set_user_preferences.dart';
import 'package:corazon_saludable/features/user_preferences/presentation/manager/user_preferences_bloc.dart';
import 'package:corazon_saludable/features/user_tips/data/data_sources/user_tips_local_data.source.dart';
import 'package:corazon_saludable/features/user_tips/data/data_sources/user_tips_remote_data_source.dart';
import 'package:corazon_saludable/features/user_tips/data/repositories/user_tips_repository_impl.dart';
import 'package:corazon_saludable/features/user_tips/domain/use_cases/get_user_tips.dart';
import 'package:corazon_saludable/features/user_tips/domain/use_cases/set_user_tips.dart';
import 'package:corazon_saludable/features/user_tips/presentation/manager/user_tips_bloc.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import 'features/user_preferences/data/data_sources/user_preferences_local_data_source.dart';
import 'features/user_preferences/data/data_sources/user_preferences_remote_data_source.dart';
import 'features/user_preferences/data/repositories/user_preferences_repository_impl.dart';
import 'features/user_preferences/domain/repositories/user_preferences_repository.dart';
import 'features/user_preferences/domain/use_cases/get_user_preferences.dart';
import 'features/user_preferences/domain/use_cases/set_user_preferences.dart';
import 'features/user_preferences/presentation/manager/user_preferences_bloc.dart';
import 'features/user_tips/data/data_sources/user_tips_local_data_source.dart';
import 'features/user_tips/data/data_sources/user_tips_remote_data_source.dart';
import 'features/user_tips/data/repositories/user_tips_repository_impl.dart';
import 'features/user_tips/domain/repositories/user_tips_repository.dart';
import 'features/user_tips/domain/use_cases/get_user_tips.dart';
import 'features/user_tips/domain/use_cases/set_user_tips.dart';
import 'features/user_tips/presentation/manager/user_tips_bloc.dart';

final sl = GetIt.instance;

///Rodrigo: with this class we are gonna manage the service locator.
///Please be careful when registering the services, there are 3 main
///methods that you might want to use, the first is registerFactory()
///to manage the Blocs, the second is registerLazySingleton, this one
///allows to instantiate the singleton only when needed, this is pretty useful
///to save resources. The third and final is registerSingleton, this method
///instantiates the singleton once the service locator is created.
Future<void> init() async {
  /// Rodrigo: inside this method all te services have to be registered
//! features - user_basic_data
// bloc
  sl.registerFactory(
    () => UserBasicInfoBloc(
      infoBasica: sl(),
      setInfoBasica: sl(),
    ),
  );
// Use cases
  //UserBasicInfo UseCase
sl.registerLazySingleton(() => GetUserBasicInfo(sl()));
sl.registerLazySingleton(() => SetUserBasicInfo(sl()));

//Repository
//TODO en el tutorial hacen una implementación con atributos nombrados,
//TODO acá se implementó con posicionales, revisar en caso de crash
sl.registerLazySingleton<UserBasicInfoRepository>(() => UserBasicInfoRepositoryImpl(sl() , sl(), sl()));

//Data Sources
sl.registerLazySingleton<UserBasicInfoRemoteDataSource>(
        () => UserBasicInfoRemoteDataSourceImpl(sl()));

sl.registerLazySingleton<UserBasicInfoLocalDataSource>(
        () => UserBasicInfoLocalDataSourceImpl(preferences: sl()));


  //UserPrefereces UseCase
  sl.registerFactory(
        () => UserPreferencesBloc(
      getPrefs: sl(),
      setPrefs: sl(),
    ),
  );
// Use cases
  sl.registerLazySingleton(() => GetUserPreferences(sl()));
  sl.registerLazySingleton(() => SetUserPreferences(sl()));

//Repository
  sl.registerLazySingleton<UserPreferencesRepository>(() => UserPreferencesRepositoryImpl(sl() , sl(), sl()));

//Data Sources
  sl.registerLazySingleton<UserPreferencesRemoteDataSource>(
          () => UserPreferenceRemoteDataSourceImpl(sl()));

  sl.registerLazySingleton<UserPreferencesLocalDataSource>(
          () => UserPreferenceLocalDataSourceImpl(preferences: sl()));
//User Tips

  //! features - user_Tips
// bloc
  sl.registerFactory(
        () => UserTipsBloc(
      getPrefs: sl(),
      setPrefs: sl(),
    ),
  );
// Use cases
  sl.registerLazySingleton(() => GetUserTips(sl()));
  sl.registerLazySingleton(() => SetUserTips(sl()));
//Repository
  sl.registerLazySingleton<UserTipsRepository>(() => UserTipsRepositoryImpl(sl() , sl(), sl()));

//Data Sources
  sl.registerLazySingleton<UserTipsRemoteDataSource>(
          () => UserTipsRemoteDataSourceImpl(sl()));

  sl.registerLazySingleton<UserTipsLocalDataSource>(
          () => UserTipsLocalDataSourceImpl(preferences: sl()));

//! core
sl.registerLazySingleton<NetworkInfo>(
        () => NetworkInfoImpl(sl()));
sl.registerLazySingleton(() => AnalyticService());
//! external services
final sharedPreferences = await SharedPreferences.getInstance();
sl.registerLazySingleton(() => sharedPreferences);
sl.registerLazySingleton(() => http.Client());
sl.registerLazySingleton(() => DataConnectionChecker());

//! features - user_Preferences
// bloc
  sl.registerFactory(
        () => UserPreferencesBloc(
      getPrefs: sl(),
      setPrefs: sl(),
    ),
  );
// Use cases
  sl.registerLazySingleton(() => GetUserPreferences(sl()));
  sl.registerLazySingleton(() => SetUserPreferences(sl()));

//Repository
  sl.registerLazySingleton<UserPreferencesRepository>(() => UserPreferencesRepositoryImpl(sl() , sl(), sl()));

//Data Sources
  sl.registerLazySingleton<UserPreferencesRemoteDataSource>(
          () => UserPreferenceRemoteDataSourceImpl(sl()));

  sl.registerLazySingleton<UserPreferencesLocalDataSource>(
          () => UserPreferenceLocalDataSourceImpl(preferences: sl()));

//! core
  sl.registerLazySingleton<NetworkInfo>(
          () => NetworkInfoImpl(sl()));
  sl.registerLazySingleton(() => AnalyticService());


  //! features - user_Tips
// bloc
  sl.registerFactory(
        () => UserTipsBloc(
      getPrefs: sl(),
      setPrefs: sl(),
    ),
  );
// Use cases
  sl.registerLazySingleton(() => GetUserTips(sl()));
  sl.registerLazySingleton(() => SetUserTips(sl()));

//Repository
  sl.registerLazySingleton<UserTipsRepository>(() => UserTipsRepositoryImpl(sl() , sl(), sl()));

//Data Sources
  sl.registerLazySingleton<UserTipsRemoteDataSource>(
          () => UserTipsRemoteDataSourceImpl(sl()));

  sl.registerLazySingleton<UserTipsLocalDataSource>(
          () => UserTipsLocalDataSourceImpl(preferences: sl()));

//! core
  sl.registerLazySingleton<NetworkInfo>(
          () => NetworkInfoImpl(sl()));
  sl.registerLazySingleton(() => AnalyticService());


}
