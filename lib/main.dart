import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:corazon_saludable/UI/authentication.dart';
import 'package:corazon_saludable/UI/sign_in_sign_up_menu.dart';
import 'package:corazon_saludable/core/firebase/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'injection_container.dart' as di;
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

import 'injection_container.dart';
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  await Firebase.initializeApp();
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
  runApp(const MyApp());
  //!metodo para consultar el estado del repo a tiempo real
  /**
   *

    try{
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: "prueba@mail.com", password: "123pormi");
      print("si se pudo burro");
    } catch(e){
      print(e);
    }
    String uid = FirebaseAuth.instance.currentUser.uid;
    DocumentSnapshot captura = await FirebaseFirestore.instance.collection('usuarios').doc(uid).get();
    if(!captura.exists){
      print("no existe captura de este doc");
    }
    else{
      print(captura.data()['nombreCompleto']);
      print(captura.data()['fechaNacimiento']);
    }

   */
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Corazón Saludable',
      home: SignInSignUpMenu(),
      navigatorObservers: [sl<AnalyticService>().getAnalyticsObserver()],
    );
  }
}