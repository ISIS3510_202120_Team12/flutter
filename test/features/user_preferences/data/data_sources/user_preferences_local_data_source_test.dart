import 'dart:convert';


import 'package:corazon_saludable/core/error/exeptions.dart';
import 'package:corazon_saludable/features/user_preferences/data/data_sources/user_preferences_local_data_source.dart';
import 'package:corazon_saludable/features/user_preferences/data/models/user_preferences_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:matcher/matcher.dart';

import '../../../../fixtures/fixture_reader.dart';


class MockSharedPreferences extends Mock implements SharedPreferences {}

void main() {
  UserPreferenceLocalDataSourceImpl? dataSource;
  MockSharedPreferences? mockSharedPreferences;

  setUp(() {
    mockSharedPreferences = MockSharedPreferences();
    dataSource = UserPreferenceLocalDataSourceImpl(preferences: mockSharedPreferences!);
  });
  group('getLastUserPreference', () {
    final tUserPreferenceModel =
    UserPreferencesModel.fromJson(json.decode(fixture('preferences_cached.json')));

    test(
      'should return USerPreferences from SharedPreferences when there is one in the cache',
          () async {
        // arrange
        when(() =>mockSharedPreferences!.getString(any()))
            .thenReturn(fixture('preferences_cached.json'));
        // act
        final result = await dataSource!.getLastUserPreference();
        // assert
        verify(() =>mockSharedPreferences!.getString(CACHED_USER_PREFERENCE));
        expect(result, equals(tUserPreferenceModel));
      },
    );

    test(
      'should throw a CacheExeption when there is not a cached value',
          () async {
        // arrange
        when(() =>mockSharedPreferences!.getString(any())).thenReturn(null);
        // act
        final call = dataSource!.getLastUserPreference;
        print(call);
        // assert
        expect(() => call(), throwsA(TypeMatcher<CacheException>()));
      },
    );
});
/*
    group('cacheUserPreference', () {
      final tUserPreferenceModel =
      UserPreferencesModel([true, true, true, true, true, true, true, true, true], 'Cra 56#24-76', true, ['Trotar', 'Futbol'], false, true);
      test(
        'should call SharedPreferences to cache the data',
            () async {
          // act
          dataSource!.cacheUserPreference(tUserPreferenceModel);
          // assert
          final expectedJsonString = json.encode(tUserPreferenceModel.toJson());
          verify(() =>mockSharedPreferences!.setString(
            CACHED_USER_PREFERENCE,
            expectedJsonString,
          ));
        },
      );
    });*/


}