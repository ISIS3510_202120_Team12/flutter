import 'dart:convert';

import 'package:corazon_saludable/core/error/exeptions.dart';
import 'package:corazon_saludable/features/user_preferences/data/data_sources/user_preferences_remote_data_source.dart';
import 'package:corazon_saludable/features/user_preferences/data/models/user_preferences_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:matcher/matcher.dart';
import 'package:http/http.dart' as http;
import 'package:mocktail/mocktail.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  UserPreferenceRemoteDataSourceImpl? dataSource;
  MockHttpClient? mockHttpClient;

  setUp(() {
    mockHttpClient = MockHttpClient();
    dataSource = UserPreferenceRemoteDataSourceImpl(mockHttpClient!);
  });

  void setUpMockHttpClientSuccess200() {
    when(() =>mockHttpClient!.get(Uri.parse("https://us-central1-backendcorazonsaludable.cloudfunctions.net/preferencias"), headers: any(named:'headers')))
        .thenAnswer((_) async => http.Response(fixture('preferences.json'), 200));
  }

  void setUpMockHttpClientFailure404() {
    when(() =>mockHttpClient!.get(Uri.parse("https://us-central1-backendcorazonsaludable.cloudfunctions.net/preferencias"), headers: any(named:'headers')))
        .thenAnswer((_) async => http.Response('Something went wrong', 404));
  }

  group('getRandomNumberTrivia', () {
    final tNumberTriviaModel =
    UserPreferencesModel.fromJson(json.decode(fixture('preferences.json')));

    test(
      '''should perform a GET request on a URL with number
       being the endpoint and with application/json header''',
          () async {
        // arrange
        setUpMockHttpClientSuccess200();
        // act
        dataSource!.getUserPreferences();
        // assert
        verify(() => mockHttpClient!.get(
          Uri.parse('https://us-central1-backendcorazonsaludable.cloudfunctions.net/preferencias'),
          headers: {
            'Content-Type': 'application/json',
          },
        ));
      },
    );

    test(
      'should return NumberTrivia when the response code is 200 (success)',
          () async {
        // arrange
        setUpMockHttpClientSuccess200();
        // act
        final result = await dataSource!.getUserPreferences();
        // assert
        expect(result, equals(tNumberTriviaModel));
      },
    );

   /* test(
      'should throw a ServerException when the response code is 404 or other',
          () async {
        // arrange
        setUpMockHttpClientFailure404();
        // act
        final call = dataSource!.getUserPreferences;
        // assert
        expect(() => call(), throwsA(TypeMatcher<ServerException>()));
      },
    );*/
  });
}