

import 'dart:convert';

import 'package:corazon_saludable/features/user_preferences/data/models/user_preferences_model.dart';
import 'package:corazon_saludable/features/user_preferences/domain/entities/user_preferences.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/fixture_reader.dart';

void main () {

  final tUserPreferencesModel = UserPreferencesModel([true, true, true, true, true, true, true, true, true], 'Cra 56#24-76', true, ['Trotar', 'Futbol'], false, true);

  test(
    'Subclasss of entity in model',
      () async {
      //assert
        expect(tUserPreferencesModel, isA<UserPreferences>());
      },
  );

    group('fromJson', () {

        test(
          'Return valid model when Jscon given',
            ()async {
            //arrange
              final Map <String, dynamic> jsonMap=
                  json.decode(fixture('preferences.json'));
              //act
              final result = UserPreferencesModel.fromJson(jsonMap);
              //assert
              expect (result, tUserPreferencesModel);
            },
        );

    });

    group('toJson', () {
      test(
          'Return Json map',
          () async {
           final result = tUserPreferencesModel.toJson();

           final expectedMap =
           {
             "vegetariano": true,
             "alergias": [
               true,
               true,
               true,
               true,
               true,
               true,
               true,
               true,
               true
             ],
             "vegano": false,
             "celiaco": true,
             "deportes": [
               "Trotar",
               "Futbol"
             ],
             "ubicacionHogar": "Cra 56#24-76"
           };
           expect(result,expectedMap);
          });

    });

}