

import 'package:corazon_saludable/core/error/exeptions.dart';
import 'package:corazon_saludable/core/error/failures.dart';
import 'package:corazon_saludable/core/network/network_info.dart';
import 'package:corazon_saludable/features/user_preferences/data/data_sources/user_preferences_local_data_source.dart';
import 'package:corazon_saludable/features/user_preferences/data/data_sources/user_preferences_remote_data_source.dart';
import 'package:corazon_saludable/features/user_preferences/data/models/user_preferences_model.dart';
import 'package:corazon_saludable/features/user_preferences/data/repositories/user_preferences_repository_impl.dart';
import 'package:corazon_saludable/features/user_preferences/domain/entities/user_preferences.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockRemoteDataSource extends Mock implements UserPreferencesRemoteDataSource{}

class MockLocalDataSource extends Mock implements UserPreferencesLocalDataSource{}

class MockNetworkInfo extends Mock implements NetworkInfo{}

void main () {
  UserPreferencesRepositoryImpl? repository;
  MockRemoteDataSource? mockRemoteDataSource;
  MockLocalDataSource? mockLocalDataSource;
  MockNetworkInfo? mockNetworkInfo;

  setUp (() {
    mockRemoteDataSource = MockRemoteDataSource();
    mockLocalDataSource = MockLocalDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = UserPreferencesRepositoryImpl(mockLocalDataSource!, mockRemoteDataSource!, mockNetworkInfo!);

  });


  void runTestsOnline(Function body) {
    group('device is online', () {
      setUp(() {
        when(() => mockNetworkInfo!.isConnected).thenAnswer((_) async => true);
      });

      body();
    });
  }

  void runTestsOffline(Function body) {
    group('device is offline', () {
      setUp(() {
        when(() => mockNetworkInfo!.isConnected).thenAnswer((_) async => false);
      });

      body();
    });
  }

  group('getUserPreferences', () {
    final tUserPreferencesModel =
    UserPreferencesModel([], "calle", true,  [],  true, true);
    final UserPreferences tUserPreferences = tUserPreferencesModel;

    test('should check if the device is online', () {
      //arrange
      when(() => mockNetworkInfo!.isConnected).thenAnswer((_) async => true);
      // act
      repository!.getUserPreferences();
      // assert
      verify(() =>mockNetworkInfo!.isConnected);
    });

    runTestsOnline(() {
      test(
        'should return remote data when the call to remote data source is successful',
            () async {
          // arrange


          when(() => mockRemoteDataSource!.getUserPreferences())
              .thenAnswer((_) async => tUserPreferencesModel);
          // act
          final result = await repository!.getUserPreferences();
          // assert
          verify(() => mockRemoteDataSource!.getUserPreferences());
          expect(result, equals(Right(tUserPreferences)));
        },
      );

      test(
        'should cache the data locally when the call to remote data source is successful',
            () async {
          // arrange
          when(() => mockRemoteDataSource!.getUserPreferences())
              .thenAnswer((_) async => tUserPreferencesModel);
          // act
          await repository!.getUserPreferences();
          // assert
          verify(() =>mockRemoteDataSource!.getUserPreferences());
          verify(() => mockLocalDataSource!.cacheUserPreference(tUserPreferencesModel));
        },
      );

      test(
        'should return server failure when the call to remote data source is unsuccessful',
            () async {
          // arrange
          when(() => mockRemoteDataSource!.getUserPreferences())
              .thenThrow(ServerException());
          // act
          final result = await repository!.getUserPreferences();
          // assert
          verify(() => mockRemoteDataSource!.getUserPreferences());
          verifyZeroInteractions(mockLocalDataSource);
          expect(result, equals(Left(ServerFailure())));
        },
      );
    });

    runTestsOffline(() {
      test(
        'should return last locally cached data when the cached data is present',
            () async {
          // arrange
          when(() => mockLocalDataSource!.getLastUserPreference())
              .thenAnswer((_) async => tUserPreferencesModel);
          // act
          final result = await repository!.getUserPreferences();
          // assert
          verifyZeroInteractions(mockRemoteDataSource);
          verify(() => mockLocalDataSource!.getLastUserPreference());
          expect(result, equals(Right(tUserPreferences)));
        },
      );

      test(
        'should return CacheFailure when there is no cached data present',
            () async {
          // arrange
          when(() =>mockLocalDataSource!.getLastUserPreference())
              .thenThrow(CacheException());
          // act
          final result = await repository!.getUserPreferences();
          // assert
          verifyZeroInteractions(mockRemoteDataSource);
          verify(() => mockLocalDataSource!.getLastUserPreference());
          expect(result, equals(Left(CacheFailure())));
        },
      );
    });
  });

}