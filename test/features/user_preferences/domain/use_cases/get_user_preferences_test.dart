

import 'package:corazon_saludable/core/usecases/usecase.dart';
import 'package:corazon_saludable/features/user_preferences/domain/entities/user_preferences.dart';
import 'package:corazon_saludable/features/user_preferences/domain/repositories/user_preferences_repository.dart';
import 'package:corazon_saludable/features/user_preferences/domain/use_cases/get_user_preferences.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
//import 'package:mocktail/mocktail.dart';


class MockUserPreferencesRepository extends Mock implements UserPreferencesRepository {}

void main() {
  GetUserPreferences? usecase;
  MockUserPreferencesRepository? mockUserPreferencesRepository;

  setUp(() {
    mockUserPreferencesRepository = MockUserPreferencesRepository();
    usecase = GetUserPreferences(mockUserPreferencesRepository!);
  });


  final tUserPrfs = UserPreferences( [], "calle", true,  [],  true, true);
  test (
  'get preferences from repo',
    () async {

    //arrange
    when(() => mockUserPreferencesRepository!.getUserPreferences())
        .thenAnswer((_) async => Right(tUserPrfs) );
      //act
    final result = await usecase!(NoParams());
    print(result);
      //assert
    expect(result, Right(tUserPrfs));
    verify(() => mockUserPreferencesRepository!.getUserPreferences());

  },
  );
}